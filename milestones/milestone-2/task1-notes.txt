Needs
	Database
	Hardware
		watches, etc.
	Garmin API

Features
	Authentication
	Graphs, Tables, representation
	scheduling of tasks, training plans

Non-functional Requirements
	storage, privacy, backup, security, usability, performance, support


User stories

1. As a coach I want to view an individual athlete's performance so that I can make personalized decisions about their future training. 
	
2. As a coach I want to view all, or some subset, of my atheletes performance/training so that I can make general team-wide training decisions. 
	
3. As a coach I want to be able to make training plans and schedules and assign workouts to individuals and/or groups, like varistity, JV, men or women, etc. so that the atheletes can be informed of upcoming workouts.
	
4. As a coach I want to create a notification about changes in schedule or training plan so that athletes can be informed. 
	
5. As an athlete I want to create an account so that I can edit my profile, bio, picture, and most importantly monitor my performance and (hopefully) improvement over time. 

6. As an athlete I want to be able to control the level of privacy so that I can limit who has access to my profile. 

7. As an admin I want to be able to create coach accounts so that they can manage their athletes. 

8. As an athlete I want to be able to choose how I am notified so that I am informed in the most convinient way to me. 
	
9. As a parent or guardian of an underage athlete I want to be able to view their profile and stats so that I can check in on them and encourage them. 

10. As a coach I want to be able to print or download graphs of performace stats or training plans so that I can reference them.  

Vision Statement 0.9
	
	
For coaches who need to keep record of their athletes overall training 
	and performance, the Place-Holder-Name is a site that will provided a single point of access 
	to view and monitor the performance of every athlete on the team, thereby allowing the coach 
	to create individualized workouts and group training tailored for the team�s needs.
 Unlike 
	the websites currently available that track athlete performance, our product will incorporate 
	data gathered from every athlete�s Garmin GPS watch, heart rate monitor, and foot pod, providing
	such information as distance, splits, time, cadence, heart rate, as well as other metrics based 
	that data. 