Needs & Features:
	Needs:
	-Database to store players, player stats, and game information.
	-API that will allow developers to create games based on eliminating other players and building player stats.
	-Web App to manage the game and players to make accounts and manage them.
	
	Features:
	-Create players to allow a user to manage their profile and level up their stats.
	-Skills that will be used to calculate a players chance of successful attacks and traps.
	-Items such as traps, range weapons, armor, anti-venom, theft
	-Bunker time to hide from enemies. 
	-Management tools to determine if eliminations are valid like photo recognition, line of sight, bluetooth.
	
Requirements:
	Non-functional:
	-Need to use Json when passing data from server to client to decrease bandwith constraints.
	-Game and player information should be stored using minimal amount of memory to conserve space.
	-Need to be able to access API from Android and IOS.
	-Database can only be accessed by server.
	-Developers must authenticate with a token when using the API.

	Functional:
	-Tables/Leader boards to display players and game matches.
	-Login page
	-Registration forms
	-Game management console
	-Admin game/player management console
	-Player profile management page
	-Game setup page