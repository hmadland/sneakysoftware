namespace Elimination.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PlayerBadge
    {
        private EliminationDBContext _eliminationDbContext;

        public PlayerBadge()
        {
        }

        public PlayerBadge(EliminationDBContext eliminationDbContext)
        {
            _eliminationDbContext = eliminationDbContext;
        }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PlayerID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int BadgeID { get; set; }
        
        public int GameID { get; set; }

        public DateTime DateEarned { get; set; }

        public virtual Badge Badge { get; set; }

        public virtual Game Game { get; set; }

        public virtual Player Player { get; set; }
    }
}
