﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Elimination.Models
{
    public class BasicStorage
    {
        private string blobConnect;
        private CloudStorageAccount storageAccount;
        private CloudBlobClient cloudBlobClient;
        public static string blobDatePattern = "-((([0-9]{2}-){2}[0-9]{2})-(([0-9]{2}-){2}[0-9]{2}))-";

        public BasicStorage()
        {
            Initialize();
        }

        /// <summary>
        /// Returns the name of the blob storage database
        /// </summary>
        public static string BlobStorageName => WebConfigurationManager.AppSettings["BlobStorageName"];

        public string BlobDatePattern { get => blobDatePattern; set => blobDatePattern = value; }

        /// <summary>
        /// Returns a Blob Uri that you can use to call ToString() on and have a url pointing to the 
        /// blob image
        /// </summary>
        /// <param name="blobContainer">Is the name of the blob container you are accessing.
        /// the blob name.</param>
        /// <param name="username"></param>
        /// <param name="extraText"></param>
        /// <returns>Returns a <seealso cref="Uri"/> object that with the <seealso cref="IListBlobItem"/> Url.</returns>
        public Uri GetBlob(string blobContainer = "default", string username = null, string extraText = "")
        {
            if(username == null)
            {
                var cloudContainer = cloudBlobClient.GetContainerReference(blobContainer);
                var item = cloudContainer.ListBlobs(username).LastOrDefault();
                return item.Uri;
            }
            else
            {
                var cloudContainer = cloudBlobClient.GetContainerReference(GetContainerName(username: username));
                return cloudContainer.GetBlockBlobReference(GetBlobName(".png", extraText, username: username)).Uri;
            }

        }

        /// <summary>
        /// Dates are added to blob names so this method retrieves the date from the blob name. Best used
        /// for filtering.
        /// </summary>
        /// <param name="url">The url for the blob name.</param>
        /// <returns></returns>
        public static DateTime GetBlobDateTime(string url)
        {
            var regex = new Regex(blobDatePattern);
            var groups = regex.Match(url).Groups;
            var dateFromBlob = regex.Match(url).Groups[2].Value;
            var timeFromBlob = regex.Match(url).Groups[4].Value.Replace('-', ':');
            var dateTime = $"{dateFromBlob} {timeFromBlob}";

            var date = DateTime.Now;

            try
            {
                date = DateTime.ParseExact(dateTime, "yy-MM-dd HH:mm:ss", null);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Debug.WriteLine($"Error is: {e.Message}");
            }

            return date;
        }

        /// <summary>
        /// Returns a Blob Uri that you can use to call ToString() on and have a url pointing to the 
        /// blob image
        /// </summary>
        /// <param name="blobContainer">Is the name of the blob container you are accessing.
        /// the blob name.</param>
        /// <returns>Returns a <seealso cref="Uri"/> object that with the <seealso cref="IListBlobItem"/> Url.</returns>
        public IEnumerable<IListBlobItem> GetBlobList(string blobContainer = "default")
        {
            var cloudContainer = cloudBlobClient.GetContainerReference(blobContainer);
            var items = cloudContainer.ListBlobs(null, true);
            return items;
        }

        /// <summary>
        /// Gets the newest Uri from a list of Uri's
        /// </summary>
        /// <param name="uris"></param>
        /// <returns></returns>
        public static Uri GetLatestUri(List<Uri> uris)
        {
            var maxDateTime = uris.Max(uri => GetBlobDateTime(uri.AbsoluteUri));
            var newDate = maxDateTime.AddMinutes(-1);

            var chosenUri = uris.FirstOrDefault(u => GetBlobDateTime(u.AbsoluteUri).CompareTo(newDate) > 0);

            return chosenUri;
        }

        /// <summary>
        /// Store a Blob to the Blob database
        /// </summary>
        /// <param name="blobContainer">Must start with a number or letter and then it could be numbers and characters
        /// and they can be separated by "-" dashes. name length must be between 3 an 63. If these rules aren't followed,
        /// a 404 error will be thrown.</param>
        /// <param name="blobName">The name of the blob to be stored. Please provide the file extension name.</param>
        /// <param name="filePath">The file path of the image or text file.</param>
        /// <returns></returns>
        public async Task<bool> StoreBlob(string blobContainer, string blobName, string filePath)
        {
            if (!ValidContainerName(blobContainer))
                return false;

            // Retrieve a reference to a container.
            var cloudContainer = cloudBlobClient.GetContainerReference(blobContainer);

            // Create the container if it doesn't already exist.
            cloudContainer.CreateIfNotExists();

            // Set the permissions so the blobs are public. 
            var permissions = new BlobContainerPermissions
            {
                PublicAccess = BlobContainerPublicAccessType.Blob
            };

            await cloudContainer.SetPermissionsAsync(permissions);

            var blockBlob = cloudContainer.GetBlockBlobReference(blobName);
            await blockBlob.UploadFromFileAsync(filePath);

            return true;
        }

        /// <summary>
        /// Checks to see if the container name is valid for Blob storage container name standards.
        /// </summary>
        /// <param name="containerName"></param>
        /// <returns></returns>
        public static bool ValidContainerName(string containerName)
        {
            const string validName = "^[a-z0-9](?!.*--)[a-z0-9-]{1,61}[a-z0-9]$";
            return Regex.IsMatch(containerName, validName);
        }

        /// <summary>
        /// Returns a formatted blob name
        /// </summary>
        /// <param name="extension">Then file extension to be used.</param>
        /// <param name="rvm">An optional <seealso cref="RegisterViewModel"/> that is used to build a Blob name.</param>
        /// <param name="player">An optional <seealso cref="Player"/> that is used to build a Blob name.</param>
        /// <param name="username"></param>
        /// <returns>An acceptable blob name as a string</returns>
        public static string CreateBlobName(string extension, RegisterViewModel rvm = null, Player player = null, string username = null)
        {
            var pattern = $"\\s|[.]";
            var blobNameGuid = Guid.NewGuid().ToString();
            var dateTime = DateTime.Now;

            if (rvm != null)
                return $"{Regex.Replace($"{rvm.UserName.ToLower()}", pattern, "")}{blobNameGuid}-{dateTime:yy-MM-dd-hh-mm-ss}-{extension}";
            else if (player != null)
                return $"{Regex.Replace($"{player.UserName.ToLower()}", pattern, "")}{blobNameGuid}-{dateTime:yy-MM-dd-hh-mm-ss}-{extension}";
            else if (username != null)
                return $"{Regex.Replace($"{username.ToLower()}", pattern, "")}{blobNameGuid}-{dateTime:yy-MM-dd-hh-mm-ss}-{extension}";
            else
                return $"{Regex.Replace($"default{extension}", pattern, "")}";
        }

        /// <summary>
        /// Returns a formatted blob name
        /// </summary>
        /// <param name="extension">Then file extension to be used.</param>
        /// <param name="rvm">An optional <seealso cref="RegisterViewModel"/> that is used to build a Blob name.</param>
        /// <param name="player">An optional <seealso cref="Player"/> that is used to build a Blob name.</param>
        /// <returns>An acceptable blob name as a string</returns>
        public static string GetBlobName(string extension, string extraText = "", RegisterViewModel rvm = null, Player player = null, string username = null)
        {
            var pattern = $"\\s|[.]";

            if (rvm != null)
                return $"{Regex.Replace($"{rvm.UserName.ToLower()}{extraText}", pattern, "")}{extension}";
            else if (player != null)
                return $"{Regex.Replace($"{player.UserName.ToLower()}{extraText}", pattern, "")}{extension}";
            else if (username != null)
                return $"{Regex.Replace($"{username.ToLower()}{extraText}", pattern, "")}{extension}";
            else
                return $"{Regex.Replace($"default{extension}", pattern, "")}";
        }

        /// <summary>
        /// Returns a formatted <seealso cref="CloudBlobContainer"/> name
        /// </summary>
        /// <param name="rvm">An optional <seealso cref="RegisterViewModel"/> 
        /// that is used to build a <seealso cref="CloudBlobContainer"/> name.</param>
        /// <param name="player">An optional <seealso cref="Player"/> that is used 
        /// to build a <seealso cref="CloudBlobContainer"/> name.</param>
        /// <returns>An acceptable <seealso cref="CloudBlobContainer"/> name as a string</returns>
        public static string GetContainerName(RegisterViewModel rvm = null, Player player = null, string username = null)
        {
            var pattern = $"\\s|[.]";
            
            if (rvm != null)
                return Regex.Replace(rvm.UserName.ToLower(), pattern, "");
            if (player != null)
                return Regex.Replace(player.UserName.ToLower(), pattern, "");
            if (username != null)
                return Regex.Replace(username.ToLower(), pattern, "");
            return "default";
        }

        /// <summary>
        /// Initializes this object to be usable after instantiation.
        /// </summary>
        private void Initialize()
        {
            try
            {
                blobConnect = ConfigurationManager.ConnectionStrings["EliminationPhotoBlob"].ConnectionString;
                storageAccount = CloudStorageAccount.Parse(blobConnect);
                cloudBlobClient = storageAccount.CreateCloudBlobClient();
            }
            catch (Exception e) { Debug.WriteLine(e.Message); }
        }

        /// <summary>
        /// Saves an image to the blob and returns the uri of the blob created
        /// </summary>
        /// <param name="p"></param>
        /// <param name="imageExt"></param>
        /// <param name="img"></param>
        /// <returns></returns>
        public static async Task<Uri> SaveImageToBlobAsync(string username, string imageExt, byte[] img, string extraText = "")
        {
            var basicStorage = new BasicStorage();

            var image = ImageConverter.byteArrayToImage(img);
            var imgArray = ImageConverter.imageToByteArray(image);

            //We use this as the image name when retrieving it from the blob storage
            var containerName = GetContainerName(username: username);
            var imageName = CreateBlobName(imageExt, username: username);

            //Store on server to be used in BasicSorage Model
            var localPath = HttpContext.Current.Server.MapPath($"~/bin/{imageName}");
            System.IO.File.WriteAllBytes(localPath, imgArray);

            //Store the image in the Blob database
            await basicStorage.StoreBlob(containerName, imageName, localPath);

            if (System.IO.File.Exists(localPath))
                System.IO.File.Delete(localPath);

            return basicStorage.GetBlob(containerName);
        }
    }
}