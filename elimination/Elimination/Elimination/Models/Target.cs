namespace Elimination.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Target
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int GameID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Player { get; set; }

        [Key]
        [Column("Target", Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TargetID { get; set; }

        public virtual Game Game { get; set; }

        public virtual Player APlayer { get; set; }

        public virtual Player TargetPlayer { get; set; }
    }
}
