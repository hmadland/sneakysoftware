﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Elimination.Models
{
    public class GameBadges
    {
        // Dictionary containing the criteria for Elimations based Badges
        private static Dictionary<int, Func<Player, Game, bool>> Badges = new Dictionary<int, Func<Player, Game, bool>>
        {
            // Win game by only eliminating one player
            { 24, (p, g) => g.Eliminations.Where(e => e.Eliminator == p.PlayerID).Count() == 1
                && CheckWinCondition(p,g) }, 
            // Win game by eliminating zero players
            { 25, (p, g) => g.Eliminations.Where(e => e.Eliminator == p.PlayerID).Count() == 0
                && CheckWinCondition(p,g) }, 
            // Win game by eliminating everyone
            { 26, (p, g) => g.Eliminations.Where(e => e.Eliminator == p.PlayerID).Count() == g.Players.Count() - 1
                && CheckWinCondition(p,g) }, 
            // Win a game
            { 27, (p, g) => CheckWinCondition(p,g) },
            // First blood
            { 28, (p, g) => g.Eliminations.Count() == 1
                && g.Eliminations.Where(e => e.Eliminator == p.PlayerID).Count() == 1 },
            // Eliminate 5 people in one game
            { 29, (p, g) => g.Eliminations.Where(e => e.Eliminator == p.PlayerID).Count() >= 5 },
            // Eliminate 10 people in one game
            { 30, (p, g) => g.Eliminations.Where(e => e.Eliminator == p.PlayerID).Count() >= 10 },
            // Eliminate 15 people in one game
            { 31, (p, g) => g.Eliminations.Where(e => e.Eliminator == p.PlayerID).Count() >= 15 },
        };

        private static int[] WinBadges = new int[] { 24, 25, 26, 27 };
        private static int[] GameEliminationBadges = new int[] { 28, 29, 30, 31, 32, 33 };

        /// <summary>
        /// Checks if <paramref name="player"/> has won <paramref name="game"/>
        /// </summary>
        /// <param name="player"></param>
        /// <param name="game"></param>
        /// <returns></returns>
        public static bool CheckWinCondition(Player player, Game game)
        {
            return game.Winner == player.PlayerID;
        }

        /// <summary>
        /// Checks if player has met the cretria for badgeID
        /// If so it awards the player the badge
        /// </summary>
        /// <param name="player"></param>
        /// <param name="badgeID"></param>
        /// <param name="game"></param>
        /// <returns></returns>
        public static bool CheckAndAwardBadge(Player player, int badgeID, Game game)
        {
            if (!player.PlayerBadges.Any(b => b.BadgeID == badgeID))
            {
                if (Badges.ContainsKey(badgeID) && Badges[badgeID](player, game))
                {
                    player.PlayerBadges.Add(new PlayerBadge()
                    {
                        PlayerID = player.PlayerID,
                        BadgeID = badgeID,
                        DateEarned = DateTime.Today,
                        GameID = game.GameID
                    });
                    return true;
                }
                return false;
            }
            return true;
        }

        /// <summary>
        /// Checks if player has met cretia for any badge
        /// and awards it if so
        /// </summary>
        /// <param name="player"></param>
        /// <param name="game"></param>
        public static void CheckGameBadges(Player player, Game game)
        {
            foreach (int badgeID in Badges.Keys)
            {
                CheckAndAwardBadge(player, badgeID, game);
            }
        }
    }
}