﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Elimination.Models
{
    public class PlayerComparisonViewModel
    {
        //the user
        public Player selfPlayer { get; set; }

        /*another player for comparison*/
        public Player player2 { get; set; }

        public PlayerComparisonViewModel(Player selfPlayer, Player player2)
        {
            this.selfPlayer = selfPlayer;
            //the other player
            this.player2 = player2;
        }
    }
}