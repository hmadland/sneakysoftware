namespace Elimination.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Attempt
    {
        public int AttemptID { get; set; }

        public int Eliminator { get; set; }

        public int Eliminated { get; set; }

        public decimal LocationCenterLong { get; set; }

        public decimal LocatoinCenterLat { get; set; }
        
        public DateTime TimeStamp { get; set; }

        public int GameID { get; set; }

        public virtual Player AttemptsEliminated { get; set; }

        public virtual Player AttemptsEliminator { get; set; }

        public virtual Game Game { get; set; }
    }
}
