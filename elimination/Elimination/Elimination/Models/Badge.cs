namespace Elimination.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Badge
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Badge()
        {
            PlayerBadges = new HashSet<PlayerBadge>();
        }

        public int BadgeID { get; set; }

        [Required]
        [StringLength(50)]
        public string BadgeName { get; set; }

        public int? BadgeRequirement { get; set; }

        [StringLength(100)]
        public string BadgeDescription { get; set; }

        public string Url { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PlayerBadge> PlayerBadges { get; set; }
    }
}
