namespace Elimination.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class EliminationDBContext : DbContext
    {
        public EliminationDBContext()
            : base("name=EliminationDBContext")
        {
        }

        public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<Attempt> Attempts { get; set; }
        public virtual DbSet<Elimination> Eliminations { get; set; }
        public virtual DbSet<Game> Games { get; set; }
        public virtual DbSet<Player> Players { get; set; }
        public virtual DbSet<PlayersList> PlayersLists { get; set; }
        public virtual DbSet<Target> Targets { get; set; }
        public virtual DbSet<Badge> Badges { get; set; }
        public virtual DbSet<PlayerBadge> PlayerBadges { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AspNetRole>()
                .HasMany(e => e.AspNetUsers)
                .WithMany(e => e.AspNetRoles)
                .Map(m => m.ToTable("AspNetUserRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserClaims)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserLogins)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<Badge>()
                .Property(e => e.BadgeName)
                .IsUnicode(false);

            modelBuilder.Entity<Badge>()
                .Property(e => e.BadgeDescription)
                .IsUnicode(false);

            modelBuilder.Entity<Badge>()
                .HasMany(e => e.PlayerBadges)
                .WithRequired(e => e.Badge)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Attempt>()
                .Property(e => e.LocationCenterLong)
                .HasPrecision(9, 6);

            modelBuilder.Entity<Attempt>()
                .Property(e => e.LocatoinCenterLat)
                .HasPrecision(9, 6);

            modelBuilder.Entity<Elimination>()
                .Property(e => e.LocationCenterLong)
                .HasPrecision(9, 6);

            modelBuilder.Entity<Elimination>()
                .Property(e => e.LocatoinCenterLat)
                .HasPrecision(9, 6);

            modelBuilder.Entity<Game>()
                .Property(e => e.LocationCenterLong)
                .HasPrecision(9, 6);

            modelBuilder.Entity<Game>()
                .Property(e => e.LocatoinCenterLat)
                .HasPrecision(9, 6);

            modelBuilder.Entity<Game>()
                .HasMany(e => e.Attempts)
                .WithRequired(e => e.Game)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Game>()
                .HasMany(e => e.Eliminations)
                .WithRequired(e => e.Game)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Game>()
                .HasMany(e => e.PlayersLists)
                .WithRequired(e => e.Game)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Game>()
                .HasMany(e => e.Targets)
                .WithRequired(e => e.Game)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Game>()
                .HasMany(e => e.Players)
                .WithMany(e => e.PlayersGame)
                .Map(m => m.ToTable("RequestingPlayers").MapLeftKey("GameID").MapRightKey("RequestingPlayer"));

            modelBuilder.Entity<Player>()
                .Property(e => e.PhotoUrl)
                .IsUnicode(false);

            modelBuilder.Entity<Player>()
                .HasMany(e => e.PlayerBadges)
                .WithRequired(e => e.Player)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Player>()
                .HasMany(e => e.AttemptsEliminated)
                .WithRequired(e => e.AttemptsEliminated)
                .HasForeignKey(e => e.Eliminated)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Player>()
                .HasMany(e => e.AttemptsEliminator)
                .WithRequired(e => e.AttemptsEliminator)
                .HasForeignKey(e => e.Eliminator)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Player>()
                .HasMany(e => e.EliminationsEliminated)
                .WithRequired(e => e.EEliminated)
                .HasForeignKey(e => e.Eliminated)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Player>()
                .HasMany(e => e.EliminationsEliminator)
                .WithRequired(e => e.EEliminator)
                .HasForeignKey(e => e.Eliminator)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Player>()
                .HasMany(e => e.Games)
                .WithRequired(e => e.Player)
                .HasForeignKey(e => e.Host)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Player>()
                .HasMany(e => e.PlayersLists)
                .WithRequired(e => e.Player)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Player>()
                .HasMany(e => e.Targets)
                .WithRequired(e => e.APlayer)
                .HasForeignKey(e => e.Player)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Player>()
                .HasMany(e => e.TargetsOfPlayer)
                .WithRequired(e => e.TargetPlayer)
                .HasForeignKey(e => e.TargetID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Player>()
                .HasMany(e => e.PFPlayers)
                .WithMany(e => e.PFFriends)
                .Map(m => m.ToTable("PlayersFriends").MapLeftKey("PlayerFiend").MapRightKey("Player"));

            modelBuilder.Entity<Player>()
                .HasMany(e => e.FRPlayers)
                .WithMany(e => e.FRRequestingPlayers)
                .Map(m => m.ToTable("FriendRequests").MapLeftKey("FriendRequestPlayer").MapRightKey("Player"));

            modelBuilder.Entity<Player>()
                .HasMany(e => e.GamesWon)
                .WithOptional(e => e.WinnerObj)
                .HasForeignKey(e => e.Winner);
        }
    }
}