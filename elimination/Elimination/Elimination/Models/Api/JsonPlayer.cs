﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Elimination.Models.Api
{
    [DataContract]
    public class JsonPlayer
    {
        [DataMember]
        public int PlayerID { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public DateTime DOB { get; set; }

        [DataMember]
        public string PhotoUrl { get; set; }

        [DataMember]
        public string Profile { get; set; }

        [DataMember]
        public string AuthUserID { get; set; }
    }
}