﻿var imageData;


/**
 * Initializes the cropit image croper
 */
$(function () {

    //Destroy and recreate Cropit
    $('#playerEditImageEditor').cropit('destroy');
    $('#playerEditImageEditor').cropit();

    //When an image loads, move it to a holder for saving
    $('img.cropit-preview-image').on('load', function () {

        $('#save-button').attr('disabled', true);
        
        var src = $('img[class="cropit-preview-image"]').attr('src');

        $('#playerEditImageHolder').attr('value', src);
        
        checkFaceStatus(src);
    });
});


/**
 * This is for displaying the original profile photo on initial load.
 */
$(function() {
    var srcImg = $('input[id="original-image-url"]').attr('value');
    var imageDisplay =
        '<img id="original-image-display" style="width: 300px; height: 300px; border: none; border-radius: 0;"/>';

    $('img[class="cropit-preview-image"]').append(imageDisplay);

    $('#original-image-display').attr('src', srcImg);
});

/**
 * Checks the face of the image and tells the user if the image is a face.
 * @param {any} image
 */
function checkFaceStatus(image) {
    $('#facestatus')[0].innerHTML = "Checking face status...";
    var img123 = getImageBytes(image);

    var a = $.ajax({
        type: "POST",
        dataType: "json",
        url: "../../Games/IsFaceAJAX",
        data: JSON.stringify({ img: img123 }),
        contentType: 'application/json; charset=utf-8',
        async: true,
        processData: false
    }).done(function () {
        face = JSON.parse(a.responseJSON);

        if (face.isFace) {
            $('#facestatus')[0].innerHTML = "Image is a face!";
            $('#save-button').attr('disabled', false);
        } else {
            $('#facestatus')[0].innerHTML = "Image is not a face. Please upload your face!";
            $('#save-button').attr('disabled', true);
        }
    });
}