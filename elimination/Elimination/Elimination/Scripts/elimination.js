

var face;

$(document).ready(function () {
    $(".clickable-row").click(function () {
        window.location = $(this).data("href");
    });
});

/*******************Add Players Stuff**********************/
$(document).ready(function () {

    //Hide the delete image button
    $('#delete-image-button').hide();

    /*datepicker*/
    $(function () {
        $("#datepicker").datepicker({
            changeMont: true
            , changeYear: true
            , yearRange: "-110:+0"
        });
    });

    // Setup the add players button in host game details
    $('#add-players-button').bind('click', function () {
        var nameIds = getSelectedPlayers();
        var gameID = $('#add-players-button').val();
        var currpid = $('#player-id-holder').attr('data-player-id');

        console.log("Making ajax call to games controller asking for players with ids: " + nameIds);

        if (nameIds === null) {
            console.log("Names are null");
        }

        $.ajax({
            type: "POST"
            , datatype: "json"
            , url: "AddPlayers"
            , data: { ids: nameIds, gid: gameID, currPID: currpid }
            , success: function (players) {
                console.log("First player is: " + players[0].UserName);
                populateGamePlayers(players);
            }
            , error: function () {

            }
        });

        console.log("The names are: " + nameIds);
    });

    $('#populate-image-button').bind('click', function () {
        console.log("Registering data");

        $.ajax({
            type: "POST"
            , datatype: "json"
            , url: "CreateABunchOfData"
            , data: { id: 1 }
            , success: function (value) {
                console.log("Data Creation is: " + value);
            }
            , error: function () {

            }
        });
    });
});

function setSelectedPlayerNames() {
    console.log("Setting names");
    $('.selectpicker').selectpicker('val', 'Jim Lame');
    $('.selectpicker').selectpicker('refresh');
}

// Send invites through email or sms
function sendInvites(sendType) {
    var gameID = "";
    if (sendType === 'email')
        gameID = $('#email-invites-button').val();
    else
        gameID = $('#sms-invites-button').val();

    var nameIds = getSelectedPlayers();
    var currpid = $('#player-id-holder').attr('data-player-id');

    console.log("Making ajax call to send emails/sms: " + nameIds);

    $.ajax({
        type: "POST"
        , datatype: "json"
        , url: "../InvitePlayers"
        , data: { gameID: gameID, sendType: sendType }
        , success: function (status) {
            console.log("Email status is: " + status);
        }
        , error: function () {

        }
    });
}


function populateGamePlayers(players) {
    $('#game-players-table').find("tr:gt(0)").remove();
    var i = 0;
    for (i = 0; i < players.length; i++) {
        var row = $('<tr>');

        var userName = $('<td>' + players[i].UserName + '</td>');
        var email = $('<td>' + players[i].Email + '</td>');
        var phone = $('<td>' + players[i].Phone + '</td>');

        row.append(userName);
        row.append(email);
        row.append(phone);
        row.append('</r>');

        $('#game-players-table').append(row);
    }
}

var listenerFile = function (e) {
    var file = e;
    function listener() {
        while (file === undefined) {
            console.log("waiting on file");
        }
        readFile(file);
    }

    listener();
}

/*==========================================================================*/
/*************************Drag drop stuff************************************/
/*==========================================================================*/

let dropArea = document.getElementById("drop-area")

    // Prevent default drag behaviors
    ;['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
        dropArea.addEventListener(eventName, preventDefaults, false);
        document.body.addEventListener(eventName, preventDefaults, false);
    })

    // Highlight drop area when item is dragged over it
    ;['dragenter', 'dragover'].forEach(eventName => {
        dropArea.addEventListener(eventName, highlight, false);
    })

    ;['dragleave', 'drop'].forEach(eventName => {
        dropArea.addEventListener(eventName, unhighlight, false);
    });

// Handle dropped files
dropArea.addEventListener('drop', handleDrop, false);

function preventDefaults(e) {
    e.preventDefault();
    e.stopPropagation();
}

function highlight(e) {
    dropArea.classList.add('highlight');
}

function unhighlight(e) {
    dropArea.classList.remove('active');
}

function handleDrop(e) {
    var dt = e.dataTransfer;
    var files = dt.files;

    handleFiles(files);
}

let uploadProgress = [];
//let progressBar = document.getElementById('progress-bar');

function initializeProgress(numFiles) {
    progressBar.value = 0;
    uploadProgress = [];

    for (let i = numFiles; i > 0; i--) {
        uploadProgress.push(0);
    }
}

function updateProgress(fileNumber, percent) {
    uploadProgress[fileNumber] = percent;
    let total = uploadProgress.reduce((tot, curr) => tot + curr, 0) / uploadProgress.length;
    console.debug('update', fileNumber, percent, total);
    progressBar.value = total;
}

function handleFiles(files) {
    //limit the images to 3 only
    var galleryImages = getGalleryImageCount();
    if (galleryImages >= 3)
        return;

    files = [...files];
    //initializeProgress(files.length);
    files.forEach(previewFile);
}

function previewFile(file) {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = function () {
        let img = document.createElement('img');
        img.src = reader.result;
        document.getElementById('gallery').appendChild(img);
        checkFaceStatus(img);
    };
    $('#delete-image-button').show();
}

// Checks whether an image is a face
function checkFaceStatus(image) {
    $('#facestatus')[0].innerHTML = "Checking face status...";
    var img123 = getImageBytes(image.src);

    var a = $.ajax({
        type: "POST",
        dataType: "json",
        url: "../Games/IsFaceAJAX",
        data: JSON.stringify({ img: img123 }),
        contentType: 'application/json; charset=utf-8',
        async: true,
        processData: false
    }).done(function () {
        face = JSON.parse(a.responseJSON);
        var galleryImages = getGalleryImageCount();

        if (face.isFace) {
            if (galleryImages > 0) {
                $('#facestatus')[0].innerHTML = "Image is a face!";
                $('#register-button')[0].disabled = false;
            } else {
                $('#facestatus')[0].innerHTML = "Image was removed!";
                $('#register-button')[0].disabled = true;
            }
        } else {
            if (galleryImages > 0) {
                $('#facestatus')[0].innerHTML = "Image is not a face. Please upload your face!";
                $('#register-button')[0].disabled = true;
            } else {
                $('#facestatus')[0].innerHTML = "Image was removed";
                $('#register-button')[0].disabled = true;
            }
        }
    });
}

function getImageBytes(image) {
    var data = "";
    //Checks to see what type of image format it is and removes the proper header
    //Adds the image extension to an invisible elements for the controller to use
    if (image.includes('data:image/jpeg;base64,')) {
        data = image.replace('data:image/jpeg;base64,', '');
        $('#PhotoExtension').attr('value', '.jpeg');
    } else if (image.includes('data:image/png;base64,')) {
        data = image.replace('data:image/png;base64,', '');
        $('#PhotoExtension').attr('value', '.png');
    } else if (image.includes('data:image/jpg;base64,')) {
        data = image.replace('data:image/jpg;base64,', '');
        $('#PhotoExtension').attr('value', '.jpg');
    } else if (image.includes('data:image/gif;base64,')) {
        data = image.replace('data:image/gif;base64,', '');
        $('#PhotoExtension').attr('value', '.gif');
    }

    return data;
}

function getGalleryImageCount() {
    var galleryImages = $('#gallery').children().length;
    return galleryImages;
}

function deleteImage() {
    $('#gallery img:last-child').remove();

    checkGallery();
}

$('#gallery').on('DOMNodeInserted DOMNodeRemoved',
    function () {
        checkGallery();
        appendImageBytesToDropArea();
    });

function checkGallery() {
    var imagesLeft = $('#gallery').children().length;

    if (imagesLeft < 1) {
        $('#delete-image-button').hide();
        $('#register-button')[0].disabled = true;
        clearFaceImageNotification();
    }
}

function clearFaceImageNotification() {
    $('#facestatus')[0].innerHTML = "";
}

function appendImageBytesToDropArea(bytes) {
    var galleryImages = $('#gallery').children();
    var lastImage = galleryImages[(galleryImages.length - 1)];

    if (galleryImages.length > 0) {
        var bytes = getImageBytes(lastImage.src);
        // Place the image inside the dropzone
        $('#Photo').attr('value', bytes);
    }
}