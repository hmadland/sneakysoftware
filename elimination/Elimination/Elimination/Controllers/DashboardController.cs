﻿using Elimination.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Elimination.Repositories;

namespace Elimination.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {
        private readonly IRepoManager _repoManager;

        public DashboardController(IRepoManager objRepository)
        {
            _repoManager = objRepository;
        }

        // GET: Dashboard
        public ActionResult Index()
        {
            string IdentityID = User.Identity.GetUserId();
            return IndexHelper(IdentityID);
        }

        //refactored for testing. pass in a string instead of AuthID
        public ActionResult IndexHelper(string IdentityID)
        {
            Player player = _repoManager.Players.GetPlayerByAuthId(IdentityID);

            if (player == null)
            {
                return HttpNotFound();
            }

            /*****get the players played/created games*****/
            List<Game> games = _repoManager.Games.GetAll().Where(g => g.Players.Any(p => p.PlayerID == player.PlayerID)).ToList();
            Game mostRecentGame = games.Where(g => g.Host != player.PlayerID).LastOrDefault();//last game played.
            int gamesPlayed = games.Where(gp => gp.Winner != null).Count();
            int gamesHosted = games.Where(gh => gh.Host == player.PlayerID).Count();

            /*get the player's eliminations*/
            List<Elimination.Models.Elimination> kills = player.EliminationsEliminator.ToList();
            List<Elimination.Models.Elimination> deaths = player.EliminationsEliminated.ToList();
            var mostRecentKill = kills.LastOrDefault();
            var mostRecentDeath = deaths.LastOrDefault();
            int totalTargetsEliminated = kills.Count();
            int totalTimesEliminated = deaths.Count();

            return View(new DashboardViewModel(player, "", games, mostRecentGame, gamesHosted, gamesPlayed, totalTargetsEliminated, totalTimesEliminated, 0));
        }

        //helper method for finding ratio
        public int Ratio(int kills, int deaths)
        {
            int ratio = 0;
            if (deaths > 0)
            {
                ratio = kills / deaths;
            }
            else
            {
                ratio = kills;

            }
            return ratio;
        }

        // GET: Dashboard
        public ActionResult Stats()
        {
            string IdentityID = User.Identity.GetUserId();
            return StatsHelper(IdentityID);
        }


        public ActionResult StatsHelper(string IdentityID)
        {
            Player player = _repoManager.Players.GetPlayerByAuthId(IdentityID);
            //number of times eliminated
            int timesEliminated = _repoManager.Eliminations.GetAll().Where(e => e.Eliminated == player.PlayerID).Count();
            //number of targets eliminated
            int targetsEliminated = _repoManager.Eliminations.GetAll().Where(e => e.Eliminator == player.PlayerID).Count();
            //number of games played
            int gamesPlayed = _repoManager.Games.GetAll().Where(g => g.Players.Any(p => p.PlayerID == player.PlayerID)).Count();
            //number of games hosted
            int host = player.Games.Where(gh => gh.Host == player.PlayerID).Count();
            //Ratio of targets eliminated and times you were eliminated
            int ratio = Ratio(targetsEliminated, timesEliminated);

            ViewBag.Ratio = ratio;
            ViewBag.Host = host;
            ViewBag.Played = gamesPlayed;
            ViewBag.TargetsEliminated = targetsEliminated;
            ViewBag.TimesEliminated = timesEliminated;
            return View();
        }

        public ActionResult UploadFaces()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> UploadFaces(byte[] image_data_1, byte[] image_data_2, byte[] image_data_3)
        {
            string ID = User.Identity.GetUserId();
            Player player = _repoManager.Players.GetPlayerByAuthId(ID);
            string username = player.UserName;

            var photoUri1 = await BasicStorage.SaveImageToBlobAsync(username, ".png", image_data_1, "_EliminPic1");
            var photoUri2 = await BasicStorage.SaveImageToBlobAsync(username, ".png", image_data_2, "_EliminPic2");
            var photoUri3 = await BasicStorage.SaveImageToBlobAsync(username, ".png", image_data_3, "_EliminPic3");

            if (photoUri1 != null && photoUri2 != null && photoUri3 != null)
                return RedirectToAction("Index");

            return View();
        }

        public ActionResult PastGames()
        {
            string IdentityID = User.Identity.GetUserId();
            return PastGamesHelper(IdentityID);
        }

        public ActionResult PastGamesHelper(string IdentityID)
        {
            Player player = _repoManager.Players.GetPlayerByAuthId(IdentityID);
            //get games that are not active
            List<Game> games = _repoManager.Games.GetAll().Where(g => g.Winner != null && g.Players.Any(p => p.PlayerID == player.PlayerID)).ToList();
            Game mostRecentGame = games.LastOrDefault();//last game played.
            int gamesPlayed = player.Games.Where(gp => gp.EndDate < DateTime.Now).Count();
            int targetsEliminated = _repoManager.Players.GetTargetsEliminatedBy(player).Count();
            int timesEliminated = player.EliminationsEliminated.Count();
            int gamesHosted = 0;
            foreach (Game g in games) //find eash game that user is a host, increment games hosted.
            {
                if (g.Host == player.PlayerID)
                {
                    gamesHosted += 1;
                }
            }
            return View(new DashboardViewModel(player, "", games, mostRecentGame, gamesHosted, gamesPlayed, targetsEliminated, timesEliminated, 0));
        }

        public ActionResult Badges()
        {
            string IdentityID = User.Identity.GetUserId();
            return BadgesHelper(IdentityID);
        }

        public ActionResult BadgesHelper(string IdentityID)
        {
            Player player = _repoManager.Players.GetPlayerByAuthId(IdentityID);
            IEnumerable<Badge> badges = GenerateBadgeList(player);
            return View(badges);
        }

        public IEnumerable<Badge> GenerateBadgeList(Player player)
        {
            IEnumerable<Badge> badges = _repoManager.Badges.GetBadgesByPlayer(player);
            return badges;
        }


        private async Task<Player> GetPlayer(int id) => await _repoManager.Players.GetPlayerByIdAsync(id);
    }
}
