﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Elimination.Models;
using Elimination.Models.Api;
using Elimination.Repositories;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using Microsoft.AspNet.Identity;

namespace Elimination.Controllers.Api
{
    public class GamesController : ApiController
    {
        private readonly IRepoManager _repoManager;

        public GamesController(IRepoManager repoManager)
        {
            _repoManager = repoManager;
        }

        /// <summary>
        /// Gets all the games
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [ResponseType(typeof(JsonGame))]
        public IHttpActionResult GetGames()
        {
            var games = _repoManager.Games.GetGames().ToList();
            if (games.Any()) return Ok(JsonBuilder.BuildGames(games));

            var response = new GeneralResponse()
            {
                Name = "API GetGames Call",
                Error = $"There are no games in the database."
            };
            return Json(response);
        }

        /// <summary>
        /// Gets all games but doesn't build navigation properties 
        /// for a much smaller JSON result
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [ResponseType(typeof(JsonGame))]
        public IHttpActionResult GetGamesLite()
        {
            var games = _repoManager.Games.GetGames().ToList();
            if (games.Any()) return Ok(JsonBuilder.BuildGamesLite(games));

            var response = new GeneralResponse()
            {
                Name = "API GetGamesLite Call",
                Error = $"There are no games in the database."
            };
            return Json(response);
        }
        
        /// <summary>
        /// Gets a game with the id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [ResponseType(typeof(JsonGame))]
        public async Task<IHttpActionResult> GetGame(int id)
        {
            var game = await _repoManager.Games.GetGameByIdAsync(id);
            if (game != null) return Ok(JsonBuilder.BuildGame(game));

            var response = new GeneralResponse()
            {
                Name = "API GetGame Call",
                Error = $"GameID {id} does not exist in the database."
            };
            return Json(response);
        }

        /// <summary>
        /// Gets a game with the id but doesn't build navigation properties 
        /// for a much smaller JSON result
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [ResponseType(typeof(JsonGame))]
        public async Task<IHttpActionResult> GetGameLite(int id)
        {
            var game = await _repoManager.Games.GetGameByIdAsync(id);
            if (game != null) return Ok(JsonBuilder.BuildGame(game));

            var response = new GeneralResponse()
            {
                Name = "API GetGameLite Call",
                Error = $"GameID {id} does not exist in the database."
            };
            return Json(response);
        }
        
        /// <summary>
        /// Gets all games hosted by player with PlayerId = pId
        /// </summary>
        /// <param name="pId"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [ResponseType(typeof(JsonGame))]
        public IHttpActionResult GetHostGames(int pId)
        {
            if (_repoManager.Players.GetPlayerByID(pId) == null)
            {
                var playerResponse = new GeneralResponse()
                {
                    Name = "API GetHostGames Call",
                    Error = $"There is no host with id of {pId}."
                };
                return Json(playerResponse);
            }

            var games = _repoManager.Games.GetGamesByPlayerId(pId).ToList();

            if (games.Any())
            {
                var gamesList = new List<JsonGame>();
                games.ForEach(g => gamesList.Add(JsonBuilder.BuildGame(g)));
                return Ok(gamesList);
            }

            var response = new GeneralResponse()
            {
                Name = "API GetHostGames Call",
                Error = $"There are no games for the host with id of {pId}."
            };
            return Json(response);
        }
        
        /// <summary>
        /// Gets a list of game from a comma seperated string of ids
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [ResponseType(typeof(JsonGame))]
        public async Task<IHttpActionResult> GetGameList(string ids)
        {
            var idArray = ids.Split(',');
            var games = await _repoManager.Games.GetGameListByIdsAsync(idArray);
            if (games == null || !games.Any())
            {
                var response = new GeneralResponse()
                {
                    Name = "API GetGameList Call",
                    Error = $"There are no games for these id's: {ids}."
                };
                return Json(response);
            }

            var jsonGames = JsonBuilder.BuildGames(games);
            return Ok(jsonGames);
            
        }

        /// <summary>
        /// Gets a list of game from a comma seperated string of ids
        /// Doesn't build navigation properties for a much smaller JSON result
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [ResponseType(typeof(JsonGame))]
        public async Task<IHttpActionResult> GetGameListLite(string ids)
        {
            var idArray = ids.Split(',');
            var games = await _repoManager.Games.GetGameListByIdsAsync(idArray);
            var jsonGames = JsonBuilder.BuildGamesLite(games);
            if (games.Any()) return Ok(jsonGames);

            var response = new GeneralResponse()
            {
                Name = "API GetGameListLite Call",
                Error = $"There are no games for these id's: {ids}."
            };
            return Json(response);
        }
        
        /// <summary>
        /// Gets all players whom are playing the game
        /// matching the id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [ResponseType(typeof(JsonGame))]
        public async Task<IHttpActionResult> GetAllPlayers(int? id)
        {
            var gameId = id ?? -1;
            var game = await _repoManager.Games.GetGameByIdAsync(gameId);

            if (game == null)
            {
                var response = new GeneralResponse()
                {
                    Name = "API GetAllPlayers Call",
                    Error = $"There is no game with the GameID of {gameId}."
                };
                return Json(response);
            }

            var jPlayers = JsonBuilder.BuildPlayers(game);
            return Ok(jPlayers);
        }

        /// <summary>
        /// Adds that player to the game
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="playerId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public async Task<IHttpActionResult> JoinGame(int gameId, int playerId)
        {
            //make Player and set it to current user
            var joiningPlayer = _repoManager.Players.GetPlayerByID(playerId);

            //Get the game
            var game = _repoManager.Games.GetGameByID(gameId);

            if (joiningPlayer == null)
            {
                var gameStr = "";
                if (game == null)
                    gameStr = $" and no game with GameID of {gameId}";
                var response = new GeneralResponse()
                {
                    Name = "API JoinGame Call",
                    Error = $"There is no player with PlayerID of: {playerId}{gameStr}."
                };
                return Json(response);
            }

            if (game == null)
            {
                var gameResponse = new GeneralResponse()
                {
                    Name = "API JoinGame Call",
                    Error = $"There is no game with the GameID of: {gameId}."
                };
                return Json(gameResponse);
            }

            var pList = new PlayersList
            {
                Game = game,
                GameID = game.GameID,
                Player = joiningPlayer,
                PlayerID = joiningPlayer.PlayerID,
                TargetCode = new Random().Next(9999)
            };

            var host = game.Host;
            var joined = game.Players;

            //if player already joined
            if (joined.Any(x => x.PlayerID == joiningPlayer.PlayerID))
            {
                var gameResponse = new GeneralResponse()
                {
                    Name = "API JoinGame Call",
                    Error = $"Player with PlayerID of {playerId} has already joined this game."
                };
                return Json(gameResponse);
            }

            // if player is host
            if (host.Equals(joiningPlayer.PlayerID))
            {
                var gameResponse = new GeneralResponse()
                {
                    Name = "API JoinGame Call",
                    Error = $"Player with id of {playerId} is the host. You cannot join your own game."
                };
                return Json(gameResponse);
            }

            //add player to game Players list
            game.PlayersLists.Add(pList);
            game.Players.Add(joiningPlayer);

            await _repoManager.SaveDbAsync();
            return Ok($"You've joined {game.Name}.");
        }
        
        /// <summary>
        /// Updates the game
        /// </summary>
        /// <param name="game"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> UpdateGame(Game game)
        {
            _repoManager.Games.UpdateState(game);

            try
            {
                await _repoManager.SaveDbAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                var response = new GeneralResponse()
                {
                    Name = "API UpdateGame Call",
                    Error = $"Couldn't update game {game.Name}."
                };
                return Json(response);
            }

            return Ok($"Updated game {game.Name}");
        }
        
        /// <summary>
        /// Adds the following game to the database
        /// </summary>
        /// <param name="game"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(Game))]
        public async Task<IHttpActionResult> PostGame(Game game)
        {
            _repoManager.Games.Add(game);
            _repoManager.Games.AddState(game);
            await _repoManager.SaveDbAsync();

            return Ok(game.GameID);
        }
        
        /// <summary>
        /// Deletes the game
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [ResponseType(typeof(Game))]
        public async Task<IHttpActionResult> DeleteGame(int id)
        {
            Game game;
            try
            {
                game = await _repoManager.Games.GetGameByIdAsync(id);
            }
            catch (Exception e)
            {
                var response = new GeneralResponse()
                {
                    Name = "API DeleteGame Call",
                    Error = $"Couldn't delete game {(string)null} because it doesn't exist. Error {e.Message}"
                };
                return Json(response);
            }

            _repoManager.Games.Remove(game);
            await _repoManager.SaveDbAsync();

            return Ok($"Deleted {game.Name}");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repoManager.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// Checks if a game exists
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public bool GameExists(int id)
        {
            return _repoManager.Games.GetGameByID(id) != null;
        }

        /// <summary>
        /// Starts the game
        /// </summary>
        /// <param name="gameId"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> StartGame(int gameId)
        {
            Game g = _repoManager.Games.Get(gameId);
            g.StartGame();
            _repoManager.Games.UpdateState(g);
            await _repoManager.SaveDbAsync();
            return Ok();
        }

        /// <summary>
        /// Returns the winner of a game
        /// </summary>
        /// <param name="gameId"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult GetWinner(int gameId)
        {
            Game g = _repoManager.Games.Get(gameId);
            return Json(new
            {
                HasGameBeenWon = g.Winner != null,
                WinnerId = g.Winner ?? -1,
                WinnerUserName = g.WinnerObj.UserName ?? "Not yet won"
            });
        }
    }
}