﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Elimination.Models;
using Elimination.Repositories;

namespace Elimination.Controllers.Api
{
    public class AttemptsController : ApiController
    {
        private EliminationDBContext db = new EliminationDBContext();
        private readonly IRepoManager _repoManager;

        public AttemptsController(IRepoManager repoManager)
        {
           _repoManager = repoManager;
        }

        // GET: api/Attempts
        public IQueryable<Attempt> GetAttempts()
        {
            return db.Attempts;
        }

        // GET: api/Attempts/5
        [ResponseType(typeof(Attempt))]
        public async Task<IHttpActionResult> GetAttempt(int id)
        {
            Attempt attempt = await db.Attempts.FindAsync(id);

            if (attempt == null)
            {
                return NotFound();
            }

            return Ok(attempt);
        }

        // PUT: api/Attempts/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAttempt(int id, Attempt attempt)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != attempt.AttemptID)
            {
                return BadRequest();
            }

            db.Entry(attempt).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AttemptExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Attempts
        [ResponseType(typeof(Attempt))]
        public async Task<IHttpActionResult> PostAttempt(Attempt attempt)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Attempts.Add(attempt);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = attempt.AttemptID }, attempt);
        }

        // DELETE: api/Attempts/5
        [ResponseType(typeof(Attempt))]
        public async Task<IHttpActionResult> DeleteAttempt(int id)
        {
            Attempt attempt = await db.Attempts.FindAsync(id);
            if (attempt == null)
            {
                return NotFound();
            }

            db.Attempts.Remove(attempt);
            await db.SaveChangesAsync();

            return Ok(attempt);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AttemptExists(int id)
        {
            return db.Attempts.Count(e => e.AttemptID == id) > 0;
        }
    }
}