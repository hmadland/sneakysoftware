﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Elimination.Models;
using Elimination.Repositories;
using Elimination.Models.Api;
using System.Web.Helpers;
using Newtonsoft.Json;

namespace Elimination.Controllers.Api
{
    public class TargetsController : ApiController
    {
        private readonly IRepoManager _repoManager;

        public TargetsController(IRepoManager objRepository)
        {
            _repoManager = objRepository;
        }

        public TargetsController()
        {
        }

        /// <summary>
        /// Returns a player that is the target of the player with <paramref name="id"/>
        /// Player does not gave navigation properties
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ResponseType(typeof(Target))]
        public async Task<IHttpActionResult> GetYourTarget(int id)
        {
            // get the player id of your target
            int tID = _repoManager.Players.GetPlayersTarget(id).FirstOrDefault();

            //Game containing both player and their target 
            var GameID = _repoManager.Players.GetGameID(id, tID).ToList();
            int gID = GameID.First();
            // targetCode of the target
            var TargetCode = _repoManager.PlayersLists.GetTargetCode(gID, tID);

            //get all target information 
            Player target = await _repoManager.Players.GetPlayerByIdAsync(tID);
            JsonPlayer jtarget = JsonBuilder.BuildPlayer(target);

            if (target == null || jtarget == null)
            {
                return NotFound();
            }

            //set FirstName to contain GameID if needed
            jtarget.FirstName = gID.ToString();
            //set LastName to contain targetCode if needed
            jtarget.LastName = TargetCode.First().ToString();

            return Ok(jtarget);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repoManager.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}