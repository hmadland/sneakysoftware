﻿using Elimination.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Elimination.Controllers
{
    public class TestController : Controller
    {
        IRepoManager gameRepository;

        public TestController(IRepoManager gameRepository)
        {
            this.gameRepository = gameRepository;
        }

        public string GetGameNameById(int id)
        {
            return gameRepository.Games.GetGameNameByID(id);
        }

        // GET: Test
        public ActionResult Index()
        {
            return View();
        }
    }
}