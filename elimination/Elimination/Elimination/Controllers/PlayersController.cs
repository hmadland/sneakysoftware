﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Elimination.Models;
using Microsoft.AspNet.Identity;
using Elimination.Repositories;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using Ninject.Infrastructure.Language;
using WebGrease.Css.Extensions;

namespace Elimination.Controllers
{
    public class PlayersController : Controller
    {
        private readonly IRepoManager _repoManager;
        private readonly string _incrementalBadgePattern = $"Eliminate-([0-9]{{1,3}})-Badge";
        private readonly string _eliminateSamePlayerPattern = $"EliminateSamePlayer-([0-9]{{1,3}})-Badge";
        private readonly string _playerEliminatedPattern = $"Eliminated-([0-9]{{1,3}})-Badge";

        public PlayersController(IRepoManager objRepository)
        {
            _repoManager = objRepository;
        }


        // GET: Players
        public ActionResult Index(int? id)
        {
            var authId = User.Identity.GetUserId();
            var currentPlayer = _repoManager.Players.GetPlayerByAuthId(authId);

            //Get all players that arent their friends
            Debug.Assert(currentPlayer != null, nameof(currentPlayer) + " != null");
            var playerNonFriends = _repoManager.Players.GetPlayersNonFriends(currentPlayer).ToList();

            //Sort the Lists
            playerNonFriends.Sort();
            currentPlayer.PFFriends.ToList().Sort();

            //Build PlayersViewModel for the View
            var playersViewModel = new PlayersViewModel
            {
                Players = playerNonFriends,
                CurrentPlayer = currentPlayer
            };

            return View(playersViewModel);
        }

        // GET: Players/Details/5
        public ActionResult Details(int id)
        {
            var identityId = User.Identity.GetUserId();


            var player = _repoManager.Players.GetPlayerByID(id);

            return View(player);
        }

        // GET: Players/Compare/5
        public ActionResult Compare(int id)
        {
            var identityId = User.Identity.GetUserId();
            var selfPlayer = _repoManager.Players.GetPlayerByAuthId(identityId);
            var otherPlayer = _repoManager.Players.GetPlayerByID(id);

            return View(new PlayerComparisonViewModel(selfPlayer, otherPlayer));
        }

        // GET: Players/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Players/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Player player)
        {
            if (!ModelState.IsValid) return View(player);

            _repoManager.Players.Add(player);
            _repoManager.SaveDb();
            return RedirectToAction("Index");

        }

        // GET: Players/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //Get player id. If it's null then set it to -1 so that we won't get a player back
            var pid = id ?? -1;

            var player = _repoManager.Players.GetPlayerByID(pid);
            if (player == null)
            {
                return HttpNotFound();
            }
            return View(player);
        }

        // POST: Players/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Player player)
        {
            var pattern = $"((data)?(:)?(image)?(\\/)?([a-z]{{3,5}})?(;)?(base64)?(,))";

            //Get the photo from the Request header
            var newPhoto = Request.Form["playerEditImageHolder"];

            //This checks if an image has been dropped and will save it
            if (newPhoto.Length > 1)
            {
                //Convert the image to a byte array
                var imageArray = ImageConverter.Base64ToBytes(newPhoto);

                //Get the photo extension
                var photoExtension = Regex.Match(newPhoto, pattern).Groups[6].Value;

                //Create the photo in blob storage and get the url
                var newUrl = await BasicStorage.SaveImageToBlobAsync(player.UserName, $".{photoExtension}", imageArray);

                var storage = new BasicStorage();

                //Get a list of all the Uri's 
                var uriList = storage.GetBlobList(BasicStorage.GetContainerName(username: player.UserName));

                //Get the newest image that was uploaded
                var newestUri = BasicStorage.GetLatestUri(uriList.Select(i => i.Uri).ToList());

                player.PhotoUrl = newestUri.AbsoluteUri;
            }

            _repoManager.Players.UpdateState(player);
            _repoManager.SaveDb();

            return RedirectToAction("Index", "Dashboard");
        }

        // GET: Players/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //Get player id if not null and -1 if null
            var pid = id ?? -1;

            var player = _repoManager.Players.GetPlayerByID(pid);

            //If player is null then return not found exception
            if (player == null) return HttpNotFound();

            return View(player);
        }

        // POST: Players/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            var player = _repoManager.Players.GetPlayerByID(id ?? -1);

            _repoManager.Players.Remove(player);
            _repoManager.SaveDb();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repoManager.Dispose();
            }
            base.Dispose(disposing);
        }


        /// <summary>
        /// This method adds a <seealso cref="Player"/> friend to a given <seealso cref="Player"/> by ID.
        /// </summary>
        /// <param name="playerId">The <seealso cref="Player"/> that the friend is being added to.</param>
        /// <param name="friendRequesterId">The <seealso cref="Player"/> that requested to be the friend.</param>
        /// <returns>A json abject with two parameters. Index 0 is the status code and index 1 is the amount of
        /// friend requests remaining. Status OK means everything went well. Status error means a general error. Status nullid means
        /// that either one or both ids were null.</returns>
        public async Task<JsonResult> AcceptFriendRequest(int? playerId, int? friendRequesterId)
        {
            var nullRequest = Json("nullid", JsonRequestBehavior.AllowGet);
            var errorRequest = Json("error", JsonRequestBehavior.AllowGet);

            /*Check if the ids are the same. If they are then return with error*/
            var pid = playerId ?? -1;
            var fpid = friendRequesterId ?? -1;

            if (pid == -1 || fpid == -1)//return with null id request
                return nullRequest;
            if (pid == fpid)
                return errorRequest;

            /*Get the player that will become the new friend of the current player*/
            var newFriend = _repoManager.Players.Get(fpid);

            /*Get the current player to add a new friend to*/
            var currentPlayer = _repoManager.Players.Get(pid);

            /*Add the new friend to current player if the new friend doesn't already exist as a friend*/
            if (currentPlayer.AddFriend(newFriend))
            {

                _repoManager.Badges.Badge(pid, 6);
                _repoManager.Players.UpdateState(currentPlayer);
                await _repoManager.SaveDbAsync();

            }
            else
            {
                return errorRequest;
            }

            string[] response = { "OK", $"{currentPlayer.FRRequestingPlayers.Count}" };

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="toBeFriend"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> RequestAFriend(int? playerId, int? toBeFriend)
        {
            var nullRequest = Json("nullid", JsonRequestBehavior.AllowGet);
            var errorRequest = Json("error", JsonRequestBehavior.AllowGet);

            /*Check if the ids are the same. If they are then return with error*/
            var pid = playerId ?? -1;
            var fpid = toBeFriend ?? -1;

            if (pid == -1 || fpid == -1) //return with null id request
                return nullRequest;
            if (pid == fpid)
                return errorRequest;

            var currentPlayer = _repoManager.Players.Get(pid);

            string[] response = { "OK", $"{toBeFriend}" };

            try
            {
                _repoManager.Badges.Badge(pid, 5);

                currentPlayer.RequestFriend(_repoManager.Players.Get(fpid));

                _repoManager.Players.UpdateState(currentPlayer);
                await _repoManager.SaveDbAsync();
            }
            catch (Exception e)
            {
                response[0] = "error";
                response[1] = "-1";
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="toBeFriend"></param>
        /// <returns></returns>
        public async Task<JsonResult> RejectFriendRequest(int? playerId, int? toBeFriend)
        {
            var nullRequest = Json("nullid", JsonRequestBehavior.AllowGet);
            var errorRequest = Json("error", JsonRequestBehavior.AllowGet);

            /*Check if the ids are the same. If they are then return with error*/
            var pid = playerId ?? -1;
            var fpid = toBeFriend ?? -1;

            if (pid == -1 || fpid == -1)//return with null id request
                return nullRequest;
            if (pid == fpid)
                return errorRequest;

            var currentPlayer = await GetPlayer(pid);

            //Check if the player had been successfully rejected. If true, then proceed to save
            //the changes and if false return
            if (currentPlayer.RejectFriend(await GetPlayer(fpid)))
            {
                _repoManager.Players.UpdateState(currentPlayer);
                _repoManager.SaveDb();
            }
            else
            {
                return Json($"Couldn't reject Player with id of: {toBeFriend}", JsonRequestBehavior.AllowGet);
            }

            string[] response = { "OK", $"{currentPlayer.FRRequestingPlayers.Count}" };

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="toBeFriendRetract"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> CancelAFriendRequest(int? playerId, int? toBeFriendRetract)
        {
            var nullRequest = Json("nullid", JsonRequestBehavior.AllowGet);
            var errorRequest = Json("error", JsonRequestBehavior.AllowGet);

            /*Check if the ids are the same. If they are then return with error*/
            var pid = playerId ?? -1;
            var fpid = toBeFriendRetract ?? -1;

            if (pid == -1 || fpid == -1) //return with null id request
                return nullRequest;
            if (pid == fpid)
                return errorRequest;

            var currentPlayer = await GetPlayer(pid);
            var friendPlayer = await GetPlayer(fpid);

            string[] response = { "OK", $"{toBeFriendRetract}" };

            try
            {
                friendPlayer = currentPlayer.CancelFriendRequest(friendPlayer);
                _repoManager.Players.UpdateState(currentPlayer);
                _repoManager.Players.UpdateState(friendPlayer);
                await _repoManager.SaveDbAsync();
            }
            catch (Exception e)
            {
                response[0] = "error";
                response[1] = "-1";
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        ///
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="toBeUnfriended"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> UnfriendPlayer(int? playerId, int? toBeUnfriended)
        {
            var nullRequest = Json("nullid", JsonRequestBehavior.AllowGet);
            var errorRequest = Json("error", JsonRequestBehavior.AllowGet);

            /*Check if the ids are the same. If they are then return with error*/
            var pid = playerId ?? -1;
            var fpid = toBeUnfriended ?? -1;

            if (pid == -1 || fpid == -1) //return with null id request
                return nullRequest;
            if (pid == fpid)
                return errorRequest;

            var currentPlayer = await GetPlayer(pid);
            var currentFriend = await GetPlayer(fpid);

            string[] response = { "OK", $"{toBeUnfriended}" };

            try
            {
                currentFriend = currentPlayer.UnfriendPlayer(currentFriend);
                _repoManager.Players.UpdateState(currentPlayer);
                _repoManager.Players.UpdateState(currentFriend);
                await _repoManager.SaveDbAsync();
            }
            catch (Exception e)
            {
                response[0] = "error";
                response[1] = "-1";
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        private async Task<Player> GetPlayer(int id) => await _repoManager.Players.GetPlayerByIdAsync(id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public JsonResult CheckForNewEliminationBadges(int? playerId)
        {
            var returnBadge = new JsonBadges
            {
                ReturnCode = "OK",
                Badges = new List<JBadge>()
            };

            var pid = playerId ?? -1;
            var authId = User.Identity.GetUserId();
            var player = _repoManager.Players.GetPlayerByID(pid);

            if (player.AuthUserID != authId)
            {
                returnBadge.ReturnCode = "error";
                returnBadge.ReturnCodeDescription = "Unregistered Player";
                return Json(returnBadge, JsonRequestBehavior.AllowGet);
            }

            //Get incremental elimination badges
            var incrementalBadges = CheckForNumberOfNewEliminationBadges(player);

            //Get incremental badges for eliminating the same player
            var samePlayerBadges = CheckForNewSamePlayerEliminationBadges(player);

            //Get incremental badges for being eliminated
            var playerEliminatedBadges = CheckForNewPlayerEliminatedBadges(player);

            if (incrementalBadges != null)
                returnBadge.AddBadges(incrementalBadges);

            if (samePlayerBadges != null)
                returnBadge.AddBadges(samePlayerBadges);

            if (playerEliminatedBadges != null)
                returnBadge.AddBadges(playerEliminatedBadges);

            return Json(returnBadge, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This searches for new incremental badges for being eliminated.
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        public List<Badge> CheckForNewPlayerEliminatedBadges(Player player)
        { 
            // This gets the amount of times this player has been eliminated
            var playerEliminatedCount = _repoManager.Players.HowManyTimesHasPlayerBeenEliminated(player);

            // Get badge increment number from database
            var badgeIncrements = GetBadgeRequirements(_playerEliminatedPattern);

            // Get any badges that the player might have
            var numberOfTimesEliminatedBadges = GetListOfCurrentBadgesByPatterName(_playerEliminatedPattern, player);

            return GetNewBadgesHelper(badgeIncrements, "Eliminated", playerEliminatedCount, numberOfTimesEliminatedBadges, player);
        }

        /// <summary>
        /// This searches for new incremental same player elimination badges.
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        public List<Badge> CheckForNewSamePlayerEliminationBadges(Player player)
        {
            //This just gets the maximum number of times the player has eliminated a target
            var samePlayerEliminations = _repoManager.Players.HowManyTimesHasPlayerEliminatedSameTarget(player);

            //Get badge increment number from database
            var badgeIncrements = GetBadgeRequirements(_eliminateSamePlayerPattern);

            //Get any badges that the player might have
            var numberOfSamePlayerEliminationBadges = GetListOfCurrentBadgesByPatterName(_eliminateSamePlayerPattern, player);

            return GetNewBadgesHelper(badgeIncrements, "EliminateSamePlayer", samePlayerEliminations, numberOfSamePlayerEliminationBadges, player);
        }

        /// <summary>
        /// This method searches for new incremental elimination badges. It will give a list of new badges that the
        /// <seealso cref="Player"/> has won based on their elimination count.
        /// </summary>
        /// <param name="player">The <seealso cref="Player"/> that we are checking for new badges.</param>
        /// <returns>A <seealso cref="List{T}"/> of new <seealso cref="Badge"/>s.</returns>
        public List<Badge> CheckForNumberOfNewEliminationBadges(Player player)
        {
            //Holds elimination count so we don't query it many times
            var eliminations = _repoManager.Players.HowManyTargetsHasPlayerEliminated(player);

            //Get the elimination badge increments as a list
            var badgeIncrements = GetBadgeRequirements(_incrementalBadgePattern);

            //Get all the incremental elimination Badges the current player has
            var numberOfEliminationBadges = GetListOfCurrentBadgesByPatterName(_incrementalBadgePattern, player);

            return GetNewBadgesHelper(badgeIncrements, "Eliminate", eliminations, numberOfEliminationBadges, player);
        }

        /// <summary>
        /// Gets a list of badges that the player has by a regular expression pattern.
        /// </summary>
        /// <param name="pattern"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public IEnumerable<Badge> GetListOfCurrentBadgesByPatterName(string pattern, Player p)
        {
            return p.PlayerBadges
                .Where(p2 => Regex.Match(p2.Badge.BadgeName, pattern, RegexOptions.Singleline).Success)
                .Select(b => b.Badge).ToList();
        }

        /// <summary>
        /// This checks to see if the player has earned any new badges. If they've earned new badges then it
        /// builds them and adds them to a list to be returned
        /// </summary>
        /// <param name="badgeIncrements">A list of integers that represent the available increments for the given badge.</param>
        /// <param name="badgePrefix">Badge names are the same except for the prefix so we just need the prefix name for
        /// any incremental badges. This rule needs to stay true for this to work.</param>
        /// <param name="eliminations">The number of eliminations the player has.</param>
        /// <param name="badges">Existing badge from player.</param>
        /// <param name="p"></param>
        /// <returns></returns>
        public List<Badge> GetNewBadgesHelper(List<int> badgeIncrements, string badgePrefix, int eliminations, IEnumerable<Badge> badges, Player p)
        {
            var newBadges = new List<Badge>();

            //Get a list of the badge elimination requirements
            var earnedBadgeIncrements = badges.Select(b => b.BadgeRequirement).ToList();

            //Check to see if a badge exists for every badge requirement
            badgeIncrements.ForEach(i =>
            {
                //No new badges 
                if (i > eliminations || earnedBadgeIncrements.Contains(i)) return;

                var badgeName = $"{badgePrefix}-{i}-Badge";
                var badgeTuple = BuildBadgeForBadges(badgeName, p);
                p.PlayerBadges.Add(badgeTuple.Item1);
                newBadges.Add(badgeTuple.Item2);
            });

            _repoManager.Players.UpdateState(p);
            _repoManager.SaveDb();

            return newBadges;
        }


        /// <summary>
        /// This builds any badges that the player has earned and returns a PlayerBadge and Badge model.
        /// </summary>
        /// <param name="badgeName">The name of the badge you want to add.</param>
        /// <param name="player">The player you want to add a badge to.</param>
        /// <param name="dateEarned"></param>
        /// <returns>A Tuple with a <seealso cref="PlayerBadge"/> and a <seealso cref="Badge"/> model.</returns>
        public Tuple<PlayerBadge, Badge> BuildBadgeForBadges(string badgeName, Player player, string dateEarned = "1/1/0001 12:00:00 AM")
        {
            //Create a new badge by the badge name
            var tempBadge = _repoManager.Badges.GetBadgeByName(badgeName);

            //We use this to send back through Json
            var playerBadge = new PlayerBadge()
            {
                PlayerID = player.PlayerID,
                BadgeID = tempBadge.BadgeID,
                Badge = tempBadge,
                GameID = -1,
                DateEarned = (dateEarned.Equals("1/1/0001 12:00:00 AM")) ? DateTime.Now.Date : DateTime.Parse(dateEarned)
            };

            return Tuple.Create(playerBadge, tempBadge);
        }

        /// <summary>
        /// Gets the increments of a certain badge. For example, elimination badges come in increments of
        /// 1, 5, 10, 25, 50, and 100 but this isn't very easily retrieved because of different badges containing
        /// increments as well. To solve this issue we just give the method a pattern to search for and make sure that
        /// group number 1 contains the numbers that we want.
        /// </summary>
        /// <param name="badgePattern">A regular expression that searches a badge name and returns its incremental numbers
        /// as a list.</param>
        /// <returns>A <seealso cref="List{T}"/> of <seealso cref="int"/>s that represent the badge increments.</returns>
        public List<int> GetBadgeRequirements(string badgePattern)
        {
            var badges = _repoManager.Badges.GetAll();
            var badgeIncrements = new List<int>();

            //For each badge, check if it's an incremental badge and that it's the one we are looking for and add the integer
            //to the list.
            badges.ForEach(b =>
            {
                var match = Regex.Match(b.BadgeName, badgePattern, RegexOptions.Singleline).Groups[1].ToString();
                if (match.Length <= 0) return;

                var badgeNumber = int.Parse(match);
                badgeIncrements.Add(badgeNumber);
            });

            return badgeIncrements;
        }
    }
}
