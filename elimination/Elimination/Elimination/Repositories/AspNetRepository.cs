﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Elimination.Models;
using Elimination.Repositories.IRepositories;

namespace Elimination.Repositories
{
    public class AspNetRepository : Repository<AspNetUser>, IAspNetRepository
    {
        public AspNetRepository(DbContext context) : base(context)
        {
        }
        
        public AspNetUser GetAspNetUserByEmail(string email) => DbContext.AspNetUsers.FirstOrDefault(x => x.Email.Equals(email));

        public string GetAspNetUserIdByEmail(string email) => DbContext.AspNetUsers.FirstOrDefault(x => x.Email.Equals(email))?.Id;

        public List<string> GetAspNetUserIdsByEmail(string email) =>
            DbContext.AspNetUsers.Where(x => x.Email.Equals(email)).Select(x => x.Id).ToList();

        public EliminationDBContext DbContext => Context as EliminationDBContext;
    }
}