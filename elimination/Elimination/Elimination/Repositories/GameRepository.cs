﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Elimination.Models;

namespace Elimination.Repositories
{
    public class GameRepository : Repository<Game>, IGameRepository
    {
        public GameRepository(DbContext context) : base(context)
        {
        }

        public Game GetGameByID(int gId) => DbContext.Games.FirstOrDefault(y => y.GameID.Equals(gId));

        public Task<Game> GetGameByIdAsync(int id) => DbContext.Games.FirstAsync(g => g.GameID == id);
        public Task<List<Game>> GetGameListByIdsAsync(string[] ids) => DbContext.Games.Where(g => ids.ToList().Contains(g.GameID.ToString())).ToListAsync();
        public List<Game> GetGameListByIds(string[] ids) => DbContext.Games.Where(g => ids.ToList().Contains(g.GameID.ToString())).ToList();

        public IEnumerable<Game> GetGamesByPlayerId(int pid) => DbContext.Players.FirstOrDefault(p => p.PlayerID == pid)?.Games;

        public IEnumerable<Game> GetGames() => DbContext.Games.ToList();

        public EliminationDBContext DbContext => Context as EliminationDBContext;

        public IEnumerable<Game> GetAllJoinableGames() => DbContext.Games.Where(g => g.Active == false && g.Winner == null);

        internal Game Get(int? id) => DbContext.Games.Find(id);

        Game IGameRepository.Get(int? id) => DbContext.Games.Find(id);

        public string GetGameNameByID(int g)
        {
            throw new NotImplementedException();
        }
    }
}
