﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Elimination.Models;

namespace Elimination.Repositories
{
    public interface IPlayersListRepository : IRepository<PlayersList>
    {
        IEnumerable<PlayersList> GetPlayerByID(int p);
        IEnumerable<PlayersList> GetPlayers();
        IEnumerable<int> GetTargetCode(int g, int p);
    }
}
