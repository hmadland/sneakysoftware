﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Elimination.Models;

namespace Elimination.Repositories
{
    public interface ITargetRepository : IRepository<Target>
    {

    }
}