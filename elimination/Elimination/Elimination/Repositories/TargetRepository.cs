﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using Elimination.Models;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using Microsoft.Data.OData.Query.SemanticAst;
using Microsoft.Owin.Security.Provider;

namespace Elimination.Repositories
{
    public class TargetRepository : Repository<Target>, ITargetRepository
    {
        public TargetRepository(DbContext context) : base(context)
        {

        }
    }
}