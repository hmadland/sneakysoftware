﻿using Elimination.Models;
using System;
using TechTalk.SpecFlow;

namespace Test
{
    [Binding]
    public class ID258Steps
    {
        Game game;

        [Given(@"I am playing a game")]
        public void GivenIAmPlayingAGame()
        {
            game = new Game();
            Player player1 = new Player() { PlayerID = 1 };
            Player player2 = new Player() { PlayerID = 2 };
            Player player3 = new Player() { PlayerID = 3 };
            game.Players.Add(player1);
            game.Players.Add(player2);
            game.Players.Add(player3);
        }

        [When(@"I win the game")]
        public void WhenIWinTheGame()
        {
            game.EliminatePlayer(2);
            game.EliminatePlayer(3);
        }

        [Then(@"I should be set as the winner")]
        public void ThenIShouldBeSetAsTheWinner()
        {
            true.Equals(game.Winner == 1);
        }

        [Given(@"I am logged into the Elimination Website")]
        public void GivenIAmLoggedIntoTheEliminationWebsite()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"I have won a game")]
        public void GivenIHaveWonAGame()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I navigate to the game's details page")]
        public void WhenINavigateToTheGameSDetailsPage()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"I should see that I have won it")]
        public void ThenIShouldSeeThatIHaveWonIt()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
