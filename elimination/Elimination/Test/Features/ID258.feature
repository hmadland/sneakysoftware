﻿Feature: ID258
	As a user, I want to know 
	when I have won a game
	so that I can be certain

@mytag
Scenario: Saving winner of a game
	Given I am playing a game
	When I win the game
	Then I should be set as the winner

Scenario: Displaying the winner
	Given I am logged into the Elimination Website
	And I have won a game
	When I navigate to the game's details page
	Then I should see that I have won it