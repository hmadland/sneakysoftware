﻿Feature: ID265
	As a developer 
	I want an API method to 
	eliminate targets by photo elimination 
	and to upload my three face photos.

@mytag
Scenario: API Eliminate Target by Photo
	Given I am playing an active game
	When I send an api post to eliminate my target by photo elimination
	And The photo I send is a match
	Then my target is eliminated 
	And I recieve back the confidence levels and that it succeeded

Scenario: API Try To Eliminate Target by Photo
	Given I am playing an active game
	When I send an api post to eliminate my target by photo elimination
	And The photo I send is not a match
	Then my target is not eliminated
	And I recieve back the confidence levels and that it did not succeed

Scenario: API Upload Three Faces
	Given I have an account
	When I send a post to upload my face photos
	And each photo is a face
	Then it should save the photos
	And it should notify me that it succeeded

Scenario: API Upload Three Faces Wrong Photos
	Given I have an account
	When I send a post to upload my face photos
	And one or more photos are not a face
	Then it should not save the photos
	And it should notify me which image is not a face