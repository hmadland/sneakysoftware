﻿Feature: ID286_ErrorPage
	In order to make the site look better
	I want custom error pages to be shown instead of defaults

@mytag
Scenario: Visit non existing page
	Given I'm at the hompage navigating to non existing page
	When I hit enter
	Then I should be returned my custom error

@mytag
Scenario: Visit an existing page
	Given I'm at the hompage navigating to an existing page
	When I press enter
	Then I should not get my error page

@mytag
Scenario: Visit Games without logging in
	Given I try visiting the "http://localhost:1842/Games" page without logging in
    When I ty to view the page
    Then I should be taken to my error page


@mytag
Scenario: Visit Games HostDetails for a game that is not mine
	Given I try visiting the "https://elimination.azurewebsites.net/Games/HostDetails/2" for a game that is not mine
    When I ty to view the page
    Then I should go to my error page

	
