﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Elimination.Controllers;
using Elimination.Models;
using Elimination.Repositories;
using Moq;
using NUnit.Framework;

namespace Test
{
    [TestFixture]
    public class AM_UnitTests
    {
        Mock<IRepoManager> _managerMock;
        TestController _testController;
        private PlayersController _playersController;

        private readonly string _incrementalBadgePattern = $"Eliminate-([0-9]{{1,3}})-Badge";
        private readonly string _eliminateSamePlayerPattern = $"EliminateSamePlayer-([0-9]{{1,3}})-Badge";
        private readonly string _playerEliminatedPattern = $"Eliminated-([0-9]{{1,3}})-Badge";

        private IEnumerable<Badge> _badges;
        private IEnumerable<Badge> _currentBadges;

        private List<int> _incrementalBadgeRequirements;
        private List<int> _eliminateSamePlayerBadgeRequirements;
        private List<int> _eliminatedBadgeRequirements;


        private const string EliminateOneBadgeName = "Eliminate-1-Badge";
        private const string EliminateFiveBadgeName = "Eliminate-5-Badge";
        private const string EliminateIncrementalPrefix = "Eliminate";

        private const string EliminateSameOneBadgeName = "EliminateSamePlayer-1-Badge";
        private const string EliminateSameFiveBadgeName = "EliminateSamePlayer-5-Badge";
        private const string EliminateSamePlayerPrefix = "EliminateSamePlayer";

        private const string EliminatedOneBadgeName = "Eliminated-1-Badge";
        private const string EliminatedFiveBadgeName = "Eliminated-5-Badge";
        private const string EliminatedPlayerPrefix = "Eliminated";

        private Badge _eliminate1Badge;
        private Badge _eliminate5Badge;

        private Badge _eliminateSame1Player;
        private Badge _eliminateSame5Players;

        private Badge _eliminated1Player;
        private Badge _eliminated5Player;

        private Player _playerAlexanderMolodyh;
        private PlayerBadge _alexanderPlayerBadge;

        private int _playerFiveEliminationCount = 5;
        private int _playerFiveEliminatedSameTargetCount = 5;
        private int _playerFiveEliminatedCount = 5;

        [SetUp]
        public void SetUpEnvironments()
        {
            _managerMock = new Mock<IRepoManager>();
            _managerMock.Setup(mm => mm.Badges.GetAll()).Returns(GetAllBadges());

            _badges = GetAllBadges();
            _incrementalBadgeRequirements = new List<int>() { 1, 5, 10, 25, 50, 100 };
            _eliminateSamePlayerBadgeRequirements = new List<int>() {1, 5, 10, 25, 50, 100};
            _eliminatedBadgeRequirements = new List<int>() { 1, 5, 20, 50 };

            _playerAlexanderMolodyh = new Player()
            {
                PlayerID = 1,
                FirstName = "Alexander",
                LastName = "Molodyh",
                UserName = "almania",
                Email = "almania@gmail.com",
                Phone = "2039203990",
                DOB = DateTime.Parse("3/12/2018 12:00:00 AM"),
                PhotoUrl = "https://eliminationphoto.blob.core.windows.net/almania/alexander molodyh.jpeg",
                Profile = "alsdkjflaskd",
                AuthUserID = "af97221c-5c3e-4809-8fd6-bbc5c9733e1c"
            };

            _eliminate1Badge = new Badge()
            {
                BadgeID = 7,
                BadgeName = EliminateOneBadgeName,
                BadgeRequirement = 1,
                BadgeDescription = "You eliminated a player",
                Url = "Images/badges/Elimination-Badges/first-elimination-96.png"
            };

            _eliminate5Badge = new Badge()
            {
                BadgeID = 8,
                BadgeName = EliminateFiveBadgeName,
                BadgeRequirement = 5,
                BadgeDescription = "You eliminated 5 players",
                Url = "Images/badges/Elimination-Badges/5-total-eliminations-96.png"
            };

            _eliminateSame1Player = new Badge()
            {
                BadgeID = 13,
                BadgeName = EliminateSameOneBadgeName,
                BadgeRequirement = 1,
                BadgeDescription = "You eliminated the same player 5 times",
                Url = "Images/badges/Elimination-Badges/first-elimination-96.png"
            };

            _eliminateSame5Players = new Badge()
            {
                BadgeID = 14,
                BadgeName = EliminateSameFiveBadgeName,
                BadgeRequirement = 5,
                BadgeDescription = "You eliminated 5 players",
                Url = "NULL"
            };

            _eliminated1Player = new Badge()
            {
                BadgeID = 18,
                BadgeName = EliminatedOneBadgeName,
                BadgeRequirement = 1,
                BadgeDescription = "You were eliminated once",
                Url = "NULL"
            };

            _eliminated5Player = new Badge()
            {
                BadgeID = 19,
                BadgeName = EliminatedFiveBadgeName,
                BadgeRequirement = 5,
                BadgeDescription = "NULL",
                Url = "NULL"
            };

            _currentBadges = new List<Badge>()
            {
                _eliminate1Badge
            };

            _alexanderPlayerBadge = new PlayerBadge()
            {
                PlayerID = _playerAlexanderMolodyh.PlayerID,
                BadgeID = 7,
                Badge = _eliminate1Badge,
                GameID = -1,
                DateEarned = DateTime.Parse("3/12/2018 12:00:00 AM")
            };

            _managerMock.Setup(mm => mm.Badges.GetBadgeByName(EliminateOneBadgeName)).Returns(_eliminate1Badge);
            _managerMock.Setup(mm => mm.Badges.GetBadgeByName(EliminateFiveBadgeName)).Returns(_eliminate5Badge);

            _managerMock.Setup(mm => mm.Badges.GetBadgeByName(EliminateSameOneBadgeName))
                .Returns(_eliminateSame1Player);

            _managerMock.Setup(mm => mm.Badges.GetBadgeByName(EliminateSameFiveBadgeName))
                .Returns(_eliminateSame5Players);

            _managerMock.Setup(mm => mm.Badges.GetBadgeByName(EliminatedOneBadgeName))
                .Returns(_eliminated1Player);

            _managerMock.Setup(mm => mm.Badges.GetBadgeByName(EliminatedFiveBadgeName))
                .Returns(_eliminated5Player);

            _managerMock.Setup(mm => mm.Players.HowManyTargetsHasPlayerEliminated(_playerAlexanderMolodyh))
                .Returns(_playerFiveEliminationCount);

            //Returns the amount of times this player eliminated the same player
            _managerMock.Setup(mm => mm.Players.HowManyTimesHasPlayerEliminatedSameTarget(_playerAlexanderMolodyh))
                .Returns(_playerFiveEliminatedSameTargetCount);

            //Returns this player being eliminated 5 times
            _managerMock.Setup(mm => mm.Players.HowManyTimesHasPlayerBeenEliminated(_playerAlexanderMolodyh))
                .Returns(_playerFiveEliminatedCount);

            _managerMock.Setup(mm => mm.Players.UpdateState(_playerAlexanderMolodyh));
            _managerMock.Setup(mm => mm.SaveDb());

            _playersController = new PlayersController(_managerMock.Object);
        }

        [Test]
        public void Test_Store_Blob_Valid_Name()
        {
            const string validContainerName = "molodyhalex";

            Assert.IsTrue(BasicStorage.ValidContainerName(validContainerName));
        }

        [Test]
        public void Test_Store_Blob_InValid_Name()
        {
            const string validContainerName = "molody--halex";

            Assert.IsFalse(BasicStorage.ValidContainerName(validContainerName));
        }

        [Test]
        public void Test_Get_Incremental_Badge_Requirements()
        {
            var listFromController = _playersController.GetBadgeRequirements(_incrementalBadgePattern);

            Assert.AreEqual(_incrementalBadgeRequirements, listFromController);
        }

        [Test]
        public void Test_Get_Times_Eliminated_Badge_Requirements()
        {
            var listFromController = _playersController.GetBadgeRequirements(_playerEliminatedPattern);

            Assert.AreEqual(_eliminateSamePlayerBadgeRequirements, listFromController);
        }
        
        [Test]
        public void Test_Get_Times_Eliminated_Same_Player_Badge_Requirements()
        {
            var listFromController = _playersController.GetBadgeRequirements(_eliminateSamePlayerPattern);

            Assert.AreEqual(_eliminatedBadgeRequirements, listFromController);
        }

        [Test]
        public void Test_Badge_Builder()
        {
            var badgeTuple = _playersController.BuildBadgeForBadges(EliminateOneBadgeName, _playerAlexanderMolodyh, "3/12/2018 12:00:00 AM");

            //was having trouble testing a PlayerBadge object so I'm testing individual properties
            Assert.AreEqual(_alexanderPlayerBadge.BadgeID, badgeTuple.Item1.BadgeID);
            Assert.AreEqual(_alexanderPlayerBadge.PlayerID, badgeTuple.Item1.PlayerID);
            Assert.AreEqual(_alexanderPlayerBadge.Badge, badgeTuple.Item1.Badge);
            Assert.AreEqual(_alexanderPlayerBadge.GameID, badgeTuple.Item1.GameID);
            Assert.AreEqual(_alexanderPlayerBadge.DateEarned, badgeTuple.Item1.DateEarned);
        }

        [Test]
        public void Test_Test_Controller()
        {
            _managerMock = new Mock<IRepoManager>();
            _managerMock.Setup(mm => mm.Games.GetGameNameByID(1)).Returns("TestGame");

            _testController = new TestController(_managerMock.Object);
            var gameName = _testController.GetGameNameById(1);

            Assert.IsTrue(gameName.Equals("TestGame"));
        }

        [Test]
        public void Test_New_Badge_Builder()
        {
            var newBadges = _playersController.GetNewBadgesHelper(_eliminateSamePlayerBadgeRequirements,
                EliminateIncrementalPrefix, _playerFiveEliminationCount, _currentBadges, _playerAlexanderMolodyh);

            Assert.AreEqual(newBadges.Count, 1);
        }

        [Test]
        public void Test_Get_List_Of_Badges_By_Pattern_Name_With_No_Badges()
        {
            var badges =
                _playersController.GetListOfCurrentBadgesByPatterName(_incrementalBadgePattern,
                    _playerAlexanderMolodyh);

            Assert.AreEqual(badges.Count(), 0);
        }

        [Test]
        public void Test_Get_List_Of_Badges_By_Pattern_Name_With_One_Badge()
        {
            _playerAlexanderMolodyh.PlayerBadges.Add(_alexanderPlayerBadge);

            var badges =
                _playersController.GetListOfCurrentBadgesByPatterName(_incrementalBadgePattern,
                    _playerAlexanderMolodyh);

            Assert.AreEqual(badges.Count(), 1);
        }

        [Test]
        public void Test_Check_For_Number_Of_New_Elimination_Badges()
        {
            var badges = _playersController.CheckForNumberOfNewEliminationBadges(_playerAlexanderMolodyh);

            Assert.AreEqual(badges.Count, 2);
        }

        [Test]
        public void Test_Check_For_New_Same_Player_Elimination_Badges()
        {
            var badges = _playersController.CheckForNewSamePlayerEliminationBadges(_playerAlexanderMolodyh);

            Assert.AreEqual(badges.Count, 2);
        }

        [Test]
        public void Test_Check_For_New_Player_Eliminated_Badges()
        {
            var badges = _playersController.CheckForNewPlayerEliminatedBadges(_playerAlexanderMolodyh);

            Assert.AreEqual(badges.Count, 2);
        }

        //[Test]
        //public void Test_Check_For_New_Badges()
        //{
        //    var badges = _playersController.CheckForNewEliminationBadges(_playerAlexanderMolodyh.PlayerID);

        //    Debug.WriteLine("Hello");
        //}

        private static IEnumerable<Badge> GetAllBadges()
        {
            return new List<Badge>()
            {
                #region badge moq

                new Badge()
                {
                    BadgeID = 1,
                    BadgeName = "RegisterBadge",
                    BadgeRequirement = 1,
                    BadgeDescription = null,
                    Url = null
                },
                new Badge()
                {
                    BadgeID = 2,
                    BadgeName = "HostBadge",
                    BadgeRequirement = 1,
                    BadgeDescription = null,
                    Url = null
                },
                new Badge()
                {
                    BadgeID = 3,
                    BadgeName = "RequestToPlayBadge",
                    BadgeRequirement = 1,
                    BadgeDescription = null,
                    Url = null
                },
                new Badge()
                {
                    BadgeID = 4,
                    BadgeName = "InvitedToGameBadge",
                    BadgeRequirement = 1,
                    BadgeDescription = null,
                    Url = null
                },
                new Badge()
                {
                    BadgeID = 5,
                    BadgeName = "FriendRequestBadge",
                    BadgeRequirement = 1,
                    BadgeDescription = null,
                    Url = null
                },
                new Badge()
                {
                    BadgeID = 6,
                    BadgeName = "MadeFriendBadge",
                    BadgeRequirement = 1,
                    BadgeDescription = null,
                    Url = null
                },
                new Badge()
                {
                    BadgeID = 7,
                    BadgeName = "Eliminate-1-Badge",
                    BadgeRequirement = 1,
                    BadgeDescription = "You eliminated a player",
                    Url = null
                },
                new Badge()
                {
                    BadgeID = 8,
                    BadgeName = "Eliminate-5-Badge",
                    BadgeRequirement = 5,
                    BadgeDescription = "You eliminated 5 players",
                    Url = null
                },
                new Badge()
                {
                    BadgeID = 9,
                    BadgeName = "Eliminate-10-Badge",
                    BadgeRequirement = 10,
                    BadgeDescription = "You eliminated 10 players",
                    Url = null
                },
                new Badge()
                {
                    BadgeID = 10,
                    BadgeName = "Eliminate-25-Badge",
                    BadgeRequirement = 25,
                    BadgeDescription = "You eliminated 25 players",
                    Url = null
                },
                new Badge()
                {
                    BadgeID = 11,
                    BadgeName = "Eliminate-50-Badge",
                    BadgeRequirement = 50,
                    BadgeDescription = "You eliminated 50 players",
                    Url = null
                },
                new Badge()
                {
                    BadgeID = 12,
                    BadgeName = "Eliminate-100-Badge",
                    BadgeRequirement = 100,
                    BadgeDescription = "You eliminated 100 players!",
                    Url = null
                },
                new Badge()
                {
                    BadgeID = 13,
                    BadgeName = "EliminateSamePlayer-1-Badge",
                    BadgeRequirement = 1,
                    BadgeDescription = "You eliminated the same player once",
                    Url = null
                },
                new Badge()
                {
                    BadgeID = 14,
                    BadgeName = "EliminateSamePlayer-5-Badge",
                    BadgeRequirement = 5,
                    BadgeDescription = "You eliminated the same player 5 times",
                    Url = null
                },
                new Badge()
                {
                    BadgeID = 15,
                    BadgeName = "EliminateSamePlayer-20-Badge",
                    BadgeRequirement = 20,
                    BadgeDescription = null,
                    Url = null
                },
                new Badge()
                {
                    BadgeID = 16,
                    BadgeName = "EliminateSamePlayer-50-Badge",
                    BadgeRequirement = 50,
                    BadgeDescription = null,
                    Url = null
                },
                new Badge()
                {
                    BadgeID = 17,
                    BadgeName = "Eliminated-1-Badge",
                    BadgeRequirement = 1,
                    BadgeDescription = null,
                    Url = null
                },
                new Badge()
                {
                    BadgeID = 18,
                    BadgeName = "Eliminated-5-Badge",
                    BadgeRequirement = 5,
                    BadgeDescription = null,
                    Url = null
                },
                new Badge()
                {
                    BadgeID = 19,
                    BadgeName = "Eliminated-10-Badge",
                    BadgeRequirement = 10,
                    BadgeDescription = null,
                    Url = null
                },
                new Badge()
                {
                    BadgeID = 20,
                    BadgeName = "Eliminated-25-Badge",
                    BadgeRequirement = 25,
                    BadgeDescription = null,
                    Url = null
                },
                new Badge()
                {
                    BadgeID = 21,
                    BadgeName = "Eliminated-50-Badge",
                    BadgeRequirement = 50,
                    BadgeDescription = null,
                    Url = null
                },
                new Badge()
                {
                    BadgeID = 22,
                    BadgeName = "Eliminated-100-Badge",
                    BadgeRequirement = 100,
                    BadgeDescription = null,
                    Url = null
                },
                new Badge()
                {
                    BadgeID = 23,
                    BadgeName = "EliminatedInGame-1-Badge",
                    BadgeRequirement = 1,
                    BadgeDescription = null,
                    Url = null
                },
                new Badge()
                {
                    BadgeID = 24,
                    BadgeName = "Eliminated-NoOne-InGame",
                    BadgeRequirement = 1,
                    BadgeDescription = null,
                    Url = null
                },
                new Badge()
                {
                    BadgeID = 25,
                    BadgeName = "FirstEliminationInGame",
                    BadgeRequirement = 1,
                    BadgeDescription = null,
                    Url = null
                },
                new Badge()
                {
                    BadgeID = 26,
                    BadgeName = "Eliminated-NoOne-InGame",
                    BadgeRequirement = 1,
                    BadgeDescription = null,
                    Url = null
                },
                new Badge()
                {
                    BadgeID = 27,
                    BadgeName = "WinGame",
                    BadgeRequirement = 1,
                    BadgeDescription = null,
                    Url = null
                }

                #endregion
            };
        }
    }
}