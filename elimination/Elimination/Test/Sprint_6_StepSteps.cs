﻿using System;
using TechTalk.SpecFlow;

namespace Test
{
    [Binding]
    public class Sprint_6_StepSteps
    {
        [Given(@"I have entered correct information to register")]
        public void GivenIHaveEnteredCorrectInformationToRegister()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"I have passed the reCaptcha")]
        public void GivenIHavePassedTheReCaptcha()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"I have not passed the reCaptcha")]
        public void GivenIHaveNotPassedTheReCaptcha()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"I select a photo using the file explorer")]
        public void GivenISelectAPhotoUsingTheFileExplorer()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"It's a photo of my face")]
        public void GivenItSAPhotoOfMyFace()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"I select (.*) photos using the file explorer")]
        public void GivenISelectPhotosUsingTheFileExplorer(int p0)
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"I select a photo using the file explorer or drag-n-drop")]
        public void GivenISelectAPhotoUsingTheFileExplorerOrDrag_N_Drop()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"I select (.*) photos using the file explorer or drag-n-drop")]
        public void GivenISelectPhotosUsingTheFileExplorerOrDrag_N_Drop(int p0)
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"They're photos of my face")]
        public void GivenTheyRePhotosOfMyFace()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"I select a photo to be my current profile picture")]
        public void GivenISelectAPhotoToBeMyCurrentProfilePicture()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"It is a photo of something that is not a face")]
        public void GivenItIsAPhotoOfSomethingThatIsNotAFace()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I press register")]
        public void WhenIPressRegister()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I press delete before it finished checking for a face")]
        public void WhenIPressDeleteBeforeItFinishedCheckingForAFace()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I press delete")]
        public void WhenIPressDelete()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I press save")]
        public void WhenIPressSave()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I try to press save")]
        public void WhenITryToPressSave()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"the result should register me and taske me to the dashboard")]
        public void ThenTheResultShouldRegisterMeAndTaskeMeToTheDashboard()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"the result should display in red letters that the reCaptcha has failed")]
        public void ThenTheResultShouldDisplayInRedLettersThatTheReCaptchaHasFailed()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"I should be registered and taken to the dashboard")]
        public void ThenIShouldBeRegisteredAndTakenToTheDashboard()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"I should see a message saying that ""(.*)""")]
        public void ThenIShouldSeeAMessageSayingThat(string p0)
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"I should the last image dropped")]
        public void ThenIShouldTheLastImageDropped()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"My profile picture should update and I should be taken back to the dashboard")]
        public void ThenMyProfilePictureShouldUpdateAndIShouldBeTakenBackToTheDashboard()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"The save button should be disabled")]
        public void ThenTheSaveButtonShouldBeDisabled()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
