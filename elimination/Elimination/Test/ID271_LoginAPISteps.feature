﻿Feature: ID271_LoginAPISteps
	As a developer, I want to be able to login through the API so I 
	can get my player's data from a previously registered account.

Background:
    Given get a player's email

Scenario: Email should be matched
	Given I use API method GetPlayerInfo
	Then PlayerID should be returned as 1


Scenario: I don't send an email
	Given I use API method GetPlayerInfo without email
	Then error message should be returned

Scenario: I send the wrong email
	Given I use API method GetPlayerInfo with wrong email
	Then I should see an error 