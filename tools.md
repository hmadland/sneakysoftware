# et progressus

## Tools

### Visual Studio
#### Microsoft Visual Studio Community 2017 
Version 15.5.6

VisualStudio.15.Release/15.5.6+27130.2027

#### Microsoft .NET Framework
Version 4.7.02556

Installed Version: Community

#### Visual Basic 2017
00369-60000-00001-AA800
Microsoft Visual Basic 2017

#### Visual C# 2017
00369-60000-00001-AA800

Microsoft Visual C# 2017

#### Visual C++ 2017   
00369-60000-00001-AA800

Microsoft Visual C++ 2017

#### Visual F# 4.1   
00369-60000-00001-AA800

Microsoft Visual F# 4.1

#### Application Insights Tools for Visual Studio Package   
8.10.01106.1

Application Insights Tools for Visual Studio

####ASP.NET and Web Tools 2017   
15.0.31127.0

ASP.NET and Web Tools 2017

#### ASP.NET Core Razor Language Services   
1.0

Provides languages services for ASP.NET Core Razor.

#### ASP.NET Web Frameworks and Tools 2017   
5.2.51007.0

#### Azure App Service Tools v3.0.0   
15.0.31106.0

Azure App Service Tools v3.0.0

#### Azure Data Lake Node   
1.0

This package contains the Data Lake integration nodes for Server Explorer.

#### Azure Data Lake Tools for Visual Studio   
2.2.9000.1

Microsoft Azure Data Lake Tools for Visual Studio

#### Azure Data Lake Tools for Visual Studio   
2.2.9000.1

Microsoft Azure Data Lake Tools for Visual Studio

#### Common Azure Tools   
1.10

Provides common services for use by Azure Mobile Services and Microsoft Azure Tools.

#### JavaScript Language Service   
2.0

JavaScript Language Service

#### Microsoft Azure HDInsight Azure Node   
2.2.9000.1

HDInsight Node under Azure Node

#### Microsoft Azure Hive Query Language Service   
2.2.9000.1

Language service for Hive query

#### Microsoft Azure Stream Analytics Language Service   
2.2.9000.1

Language service for Azure Stream Analytics

#### Microsoft Azure Stream Analytics Node   
1.0

Azure Stream Analytics Node under Azure Node

#### Microsoft Azure Tools   
2.9

Microsoft Azure Tools for Microsoft Visual Studio 2017 - v2.9.51120.3

#### Microsoft Continuous Delivery Tools for Visual Studio   
0.3

Simplifying the configuration of continuous build integration and continuous build delivery from within the Visual Studio IDE.

#### Microsoft JVM Debugger   
1.0

Provides support for connecting the Visual Studio debugger to JDWP compatible Java Virtual Machines

#### Microsoft MI-Based Debugger   
1.0

Provides support for connecting Visual Studio to MI compatible debuggers

#### Microsoft Visual C++ Wizards   
1.0

Microsoft Visual C++ Wizards

#### Microsoft Visual Studio Tools for Containers   
1.1

Develop, run, validate your ASP.NET Core applications in the target environment. F5 your application directly into a container with debugging, or CTRL + F5 to edit & refresh your app without having to rebuild the container.

#### Microsoft Visual Studio VC Package   
1.0

Microsoft Visual Studio VC Package

#### NuGet Package Manager   
4.5.0

NuGet Package Manager in Visual Studio. For more information about NuGet, visit http://docs.nuget.org/.

#### SQL Server Data Tools   
15.1.61710.120

Microsoft SQL Server Data Tools

#### ToolWindowHostedEditor   
1.0

Hosting json editor into a tool window

#### TypeScript Tools   
15.5.11025.1

TypeScript Tools for Microsoft Visual Studio

#### Visual Studio Code Debug Adapter Host Package   
1.0

Interop layer for hosting Visual Studio Code debug adapters in Visual Studio

