﻿
$(function () {
     $('.datetimepicker').datetimepicker();
});


function athIDSelect() {
    var athID = $("#AthleteID option:selected").val();
    console.log("AthleteID is: " + athID);

    $.ajax({
        type: "get",
        datatype: "json",
        url: "UpdateFileIDList",
        data: { athID },
        success: function (data) {
            var i;
            for (i = 0; i < data.length; i++) {
                console.log("data length is: " + data.length);
            }
            printChildren(data);
        },
        error: function () {
            console.log("Error ocurred");
        }
    });
}


function printChildren(ids) {
    var list = document.getElementById("FileID");
    $("#FileID").empty();

    for (i = 0; i < ids.length; i++) {
        $("#FileID").append($("<option value=" + ids[i].Value + ">" + ids[i].Text + "</option>"));
    }
}