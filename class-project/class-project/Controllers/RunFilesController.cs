﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using class_project.Models;

namespace class_project.Controllers
{
    public class RunFilesController : Controller
    {
        private ClassProjectContext db = new ClassProjectContext();

        // GET: RunFiles
        public ActionResult Index()
        {
            var runFiles = db.RunFiles.Include(r => r.Athlete);
            return View(runFiles.ToList());
        }

        // GET: RunFiles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RunFile runFile = db.RunFiles.Find(id);
            if (runFile == null)
            {
                return HttpNotFound();
            }
            return View(runFile);
        }

        // GET: RunFiles/Create
        public ActionResult Create()
        {
            ViewBag.AthleteID = new SelectList(db.Athletes, "AthleteID", "FirstName");
            return View();
        }

        // POST: RunFiles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "FileID,AthleteID,FitFilePath")] RunFile runFile)
        {
            if (ModelState.IsValid)
            {
                db.RunFiles.Add(runFile);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AthleteID = new SelectList(db.Athletes, "AthleteID", "FirstName", runFile.AthleteID);
            return View(runFile);
        }

        // GET: RunFiles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RunFile runFile = db.RunFiles.Find(id);
            if (runFile == null)
            {
                return HttpNotFound();
            }
            ViewBag.AthleteID = new SelectList(db.Athletes, "AthleteID", "FirstName", runFile.AthleteID);
            ViewBag.Name = $"{runFile.Athlete.FirstName} {runFile.Athlete.LastName}";
            return View(runFile);
        }

        // POST: RunFiles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "FileID,AthleteID,FitFilePath")] RunFile runFile)
        {
            if (ModelState.IsValid)
            {
                db.Entry(runFile).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AthleteID = new SelectList(db.Athletes, "AthleteID", "FirstName", runFile.AthleteID);
            return View(runFile);
        }

        // GET: RunFiles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RunFile runFile = db.RunFiles.Find(id);
            if (runFile == null)
            {
                return HttpNotFound();
            }
            return View(runFile);
        }

        // POST: RunFiles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RunFile runFile = db.RunFiles.Find(id);
            db.RunFiles.Remove(runFile);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
