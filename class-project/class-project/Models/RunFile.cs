namespace class_project.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RunFile
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public RunFile()
        {
            Runs = new HashSet<Run>();
        }

        [Key]
        public int FileID { get; set; }

        public int AthleteID { get; set; }

        [Required]
        [StringLength(300)]
        public string FitFilePath { get; set; }

        public virtual Athlete Athlete { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Run> Runs { get; set; }
    }
}
