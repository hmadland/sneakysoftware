# Sneaky Software
###### (Formally known as "et progressus")

## About this Repository
This is the repository for **Sneaky Software**, a student development team from the CS461 &amp; CS462 Senior Project 2017 at Western Oregon University. This project is meant as a capstone project for a Bachelor's degree in Computer Science.

### Repository Contents 
* #### Elimination Framework (Team Project)
    * Primary group project for this repository.
* #### Class Project
    * Practice, class-wide project.
* #### Milestones
    * Milestone 1: Initial Team Formation Documentation &amp;Team Project Ideas
    * Milestone 2: Class Project Inception Documentation &amp; Refined Team Project Ideas
    * Milestone 3: Class Project Refinement, Mini-Sprint 1 *amp; Team Project Inception I
    * Milestone 4: Class Project Sprint II &amp; Team Project Inception II
    * Milestone 5: Class Project Retrospective &amp; Team Project Inception III

## The Elimination Framework &amp; API

### Vision statement 1.1

---

> For people who want to create and play elimination-based live-action mobile games (like Assassin! or Humans Vs. Zombies), the Elimination Framework and API is an web application (and associated mobile apps) that allows for a host to make a game, set rules, and invite other users to join the game. Elimination-based live action games involve players being assigned other players as targets and then proceeding to attempt to eliminate their target/targets, traditionally with mock-projectiles (like Nerf-guns or balled up socks), by whatever rules are established for the specific game, until either the last player/team is remaining. Unlike current methods for playing elimination-based live-action games, our full website will remove the subjectivity that comes from a human moderator and human players determining a successful elimination and also make adding more rules and features (like player skill modifiers, a player inventory, and methods of elimination) to games easier and more fun. The application will store user accounts, skills, and stats from previous and on-going games, as well as make decisions for furthering current games and setting up new ones.

---

### Contribution
If you want to contribute, please read the [guidelines](guidelines.md) first. Here is a list of [contributors](contributors.md). 

---

### The Development Team

* #### [Hannah Madland](https://hmadland.github.io/460/)
* #### [Blake Bauer](https://bbauer15.github.io/CS460/)
* #### [Alex Molodyh](https://alexmolodyh.github.io/)
* #### [Michael Brown](https://mgeorgebrown89.github.io/)

---

### Software Construction Process

For this project we are using **Scrum Agile** processes, specifically those outlined in the Disciplined Agile Delivery process framework. We are doing six 2-week sprints, including Daily Scrum meetings, bi-weekly sprint planning meetings, review meetings, and retrospectives. Here is our [VSTS](https://almania.visualstudio.com/Elimination) scrum board. 

---

### Tools

Here is a [list](tools.md) of all the software, libraries, tools, packages, and versions used on this project. Make sure you are using the same ones to avoid any compatibility errors.

---

### Project Planning

##### Database ER Diagram

![](milestones/sprint-6/Elimination_ER_Diagram2.PNG?raw=true)

---

 Deployed Site

 Develop Site